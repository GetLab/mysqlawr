/**   
* @Title: OverviewDAO.java 
* @Package com.medsoft.perfstat.dao 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月16日 下午7:53:49 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import java.util.List;
import java.util.Map;

import com.medsoft.perfstat.pojo.ISGlobalStatus;
import com.medsoft.perfstat.pojo.ISGlobalStatusChange;
import com.medsoft.perfstat.pojo.ISGlobalVariable;
import com.medsoft.perfstat.pojo.ISIndexStatistics;
import com.medsoft.perfstat.pojo.ISTableStatistics;
import com.medsoft.perfstat.pojo.OsInfo;
import com.medsoft.perfstat.pojo.PSEventsWaitsSummaryGlobalByEventName;
import com.medsoft.perfstat.pojo.SlaveStatus;
import com.medsoft.perfstat.pojo.Top10ObjectIOWait;
import com.medsoft.perfstat.pojo.Top10SQL;
import com.medsoft.perfstat.pojo.TopSlowQueryLogDigest;

/**
 * @author zjhua
 *
 */
public interface OverviewDAO {
	List<ISGlobalVariable> queryHead(Map<String, String> param);
	List<ISGlobalVariable> queryCritialVariales(Map<String, String> param);
	List<ISGlobalVariable> querySlaveVariables(Map<String, String> param);
	List<ISGlobalVariable> queryGaleraClusterVariales(Map<String, String> param);
	List<ISGlobalVariable> queryPerfSchema(Map<String, String> param);
	
	List<ISGlobalStatus> queryEfficiency(Map<String, String> param);
	List<ISGlobalStatusChange> queryCriticalStatus(Map<String, String> param);
	List<ISGlobalStatus> queryGaleraClusterInfo(Map<String, String> param);
	
	List<Top10SQL> queryTop10ElapsedSQL(Map<String, String> param);
	List<Top10SQL> queryTop10RowsExamSQL(Map<String, String> param);
	List<Top10ObjectIOWait> queryTop10TableIOWait(Map<String, String> param);
	List<Top10ObjectIOWait> queryTop10IndexIOWait(Map<String, String> param);
	List<ISIndexStatistics> queryTop10IndexRowsRead(Map<String, String> param);
	List<ISTableStatistics> queryTop10TableRowsRead(Map<String, String> param);
	List<SlaveStatus> querySlaveStatus(Map<String, String> params);
	List<OsInfo> queryOsInfo(Map<String, String> map);
	ISGlobalStatus getGlobalStatus(String appname, String snapId,
			String string);
	List<PSEventsWaitsSummaryGlobalByEventName> queryTop10EventDetail(
			Map<String, String> params);
	List<PSEventsWaitsSummaryGlobalByEventName> queryTop10EventSummary(
			Map<String, String> params);
	List<TopSlowQueryLogDigest> queryTop10SlowQueryLogDigest(
			Map<String, String> map);
}
