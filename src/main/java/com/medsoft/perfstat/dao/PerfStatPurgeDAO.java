/**   
* @Title: MetadataUpdateDAO.java 
* @Package com.medsoft.logpool.core 
* @Description: (用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:10:29 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import com.medsoft.perfstat.pojo.AppSnap;

/**
 * @author zjhua
 *
 */
public interface PerfStatPurgeDAO {
	void deleteAppSnaps(String lastWeekDate);
	void deleteISGlobalStatus(String lastWeekDate);
	void deleteISGlobalVariables(String lastWeekDate);
	void deletePSTableIOWaitsSummaryByTable(String lastWeekDate);
	void deletePSTableLockWaitsSummaryByTable(String lastWeekDate);
	void deletePSTableIOWaitsSummaryByIndexUsage(String lastWeekDate);
	void deletePSEventsStatementsSummaryByDigest(String lastWeekDate);
	void deleteISIndexStatistics(String lastWeekDate);
	void deletePSEventsWaitsSummaryGlobalByEventName(String lastWeekDate);
	void deleteISTableStatistics(String lastWeekDate);
	void deleteSlowQuerySummary(String lastWeekDate);
	String getLastWeekDate();
	void deleteAppSnaps(AppSnap appSnap);
	void deletePSEventsWaitsSummaryGlobalByEventName(AppSnap appSnap);
	void deletePSTableIOWaitsSummaryByTable(AppSnap appSnap);
	void deleteSlowQuerySummary(AppSnap appSnap);
	void deletePSEventsStatementsSummaryByDigest(AppSnap appSnap);
	void deleteISGlobalVariables(AppSnap appSnap);
	void deleteISGlobalStatus(AppSnap appSnap);
	void deleteISIndexStatistics(AppSnap appSnap);
	void deleteISTableStatistics(AppSnap appSnap);
	void deletePSTableIOWaitsSummaryByIndexUsage(AppSnap appSnap);
	void deletePSTableLockWaitsSummaryByTable(AppSnap appSnap);
}
