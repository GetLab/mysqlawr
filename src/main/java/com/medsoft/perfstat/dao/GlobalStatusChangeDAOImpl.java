/**   
* @Title: GlobalStatusChangeDAOImpl.java 
* @Package com.medsoft.perfstat.dao 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月17日 上午10:19:31 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.medsoft.perfstat.pojo.ISGlobalStatusChange;

/**
 * @author zjhua
 *
 */
@Service
public class GlobalStatusChangeDAOImpl implements GlobalStatusChangeDAO {

private static final String DAO_NAMESPACE = "com.medsoft.perfstat.mybatis.Status.";
	
	@Autowired
	@Qualifier("sqlSession")
	private SqlSession sqlSession;
	
	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.GlobalStatusChangeDAO#queryGlobalStatusChange()
	 */
	@Override
	public List<ISGlobalStatusChange> queryGlobalStatusChange(Map<String,String> param) {
		return sqlSession.selectList(DAO_NAMESPACE + "query", param);
	}

}
