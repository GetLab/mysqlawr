/**   
* @Title: GlobalVariablesChangeDAOImpl.java 
* @Package com.medsoft.perfstat.dao 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月17日 上午10:19:57 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.medsoft.perfstat.pojo.ISGlobalVariableChange;

/**
 * @author zjhua
 *
 */
@Service
public class GlobalVariablesChangeDAOImpl implements GlobalVariablesChangeDAO {

private static final String DAO_NAMESPACE = "com.medsoft.perfstat.mybatis.Variable.";
	
	@Autowired
	@Qualifier("sqlSession")
	private SqlSession sqlSession;
	
	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.GlobalVariablesChangeDAO#queryGlobalStatusChange()
	 */
	@Override
	public List<ISGlobalVariableChange> queryGlobalVariablesChange(Map<String,String> param) {
		return sqlSession.selectList(DAO_NAMESPACE + "query", param);
	}

}
