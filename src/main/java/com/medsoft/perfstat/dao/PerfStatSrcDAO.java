/**   
* @Title: MetadataUpdateDAO.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:10:29 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import java.util.List;

import com.medsoft.perfstat.pojo.ISGlobalStatus;
import com.medsoft.perfstat.pojo.ISGlobalVariable;
import com.medsoft.perfstat.pojo.ISIndexStatistics;
import com.medsoft.perfstat.pojo.ISTableStatistics;
import com.medsoft.perfstat.pojo.InnodbBufferPoolStats;
import com.medsoft.perfstat.pojo.PSEventsStatementsSummaryByDigest;
import com.medsoft.perfstat.pojo.PSEventsWaitsSummaryGlobalByEventName;
import com.medsoft.perfstat.pojo.PSTableIOWaitsSummaryByIndexUsage;
import com.medsoft.perfstat.pojo.PSTableIOWaitsSummaryByTable;
import com.medsoft.perfstat.pojo.PSTableLockWaitsSummaryByTable;
import com.medsoft.perfstat.pojo.SlaveStatus;

/**
 * @author zjhua
 *
 */
public interface PerfStatSrcDAO {
	List<ISGlobalStatus> queryISGlobalStatus();
	List<ISGlobalVariable> queryISGlobalVariables();
	List<ISGlobalStatus> queryISGlobalStatus57();
	List<ISGlobalVariable> queryISGlobalVariables57();
	List<PSTableIOWaitsSummaryByTable> queryPSTableIOWaitsSummaryByTable();
	List<PSTableLockWaitsSummaryByTable> queryPSTableLockWaitsSummaryByTable();
	List<PSTableIOWaitsSummaryByIndexUsage> queryPSTableIOWaitsSummaryByIndexUsage();
	List<PSEventsStatementsSummaryByDigest> queryPSEventsStatementsSummaryByDigest();
	List<ISIndexStatistics> queryISIndexStatistics();
	List<PSEventsWaitsSummaryGlobalByEventName> queryPSEventsWaitsSummaryGlobalByEventName();
	List<ISTableStatistics> queryISTableStatistics();
	
	List<InnodbBufferPoolStats> queryInnodbBufferPoolStats();
	List<SlaveStatus> querySlaveStatus();
	void disableSlowLog();
	void enableSlowLog();
	String getSlowFileName();
	String getSlowFileName57();
}
