/**   
* @Title: OverviewDAOImpl.java 
* @Package com.medsoft.perfstat.dao 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月17日 上午9:41:50 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.medsoft.perfstat.pojo.ISGlobalStatus;
import com.medsoft.perfstat.pojo.ISGlobalStatusChange;
import com.medsoft.perfstat.pojo.ISGlobalVariable;
import com.medsoft.perfstat.pojo.ISIndexStatistics;
import com.medsoft.perfstat.pojo.ISTableStatistics;
import com.medsoft.perfstat.pojo.OsInfo;
import com.medsoft.perfstat.pojo.PSEventsWaitsSummaryGlobalByEventName;
import com.medsoft.perfstat.pojo.SlaveStatus;
import com.medsoft.perfstat.pojo.Top10ObjectIOWait;
import com.medsoft.perfstat.pojo.Top10SQL;
import com.medsoft.perfstat.pojo.TopSlowQueryLogDigest;

/**
 * @author zjhua
 *
 */
@Service
public class OverviewDAOImpl implements OverviewDAO {
	private static final String DAO_NAMESPACE = "com.medsoft.perfstat.mybatis.Overview.";
	
	@Autowired
	@Qualifier("sqlSession")
	private SqlSession sqlSession;

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryHead(Map<String, String> params)
	 */
	@Override
	public List<ISGlobalVariable> queryHead(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryHead",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryCritialVariales(Map<String, String> params)
	 */
	@Override
	public List<ISGlobalVariable> queryCritialVariales(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryCritialVariales",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#querySlaveVariables(Map<String, String> params)
	 */
	@Override
	public List<ISGlobalVariable> querySlaveVariables(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "querySlaveVariables",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryGaleraClusterVariales(Map<String, String> params)
	 */
	@Override
	public List<ISGlobalVariable> queryGaleraClusterVariales(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryGaleraClusterVariales",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryPerfSchema(Map<String, String> params)
	 */
	@Override
	public List<ISGlobalVariable> queryPerfSchema(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryPerfSchema",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryEfficiency(Map<String, String> params)
	 */
	@Override
	public List<ISGlobalStatus> queryEfficiency(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryEfficiency",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryCriticalStatus(Map<String, String> params)
	 */
	@Override
	public List<ISGlobalStatusChange> queryCriticalStatus(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryCriticalStatus",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryGaleraClusterInfo(Map<String, String> params)
	 */
	@Override
	public List<ISGlobalStatus> queryGaleraClusterInfo(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryGaleraClusterInfo",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryTop10ElapsedSQL(Map<String, String> params)
	 */
	@Override
	public List<Top10SQL> queryTop10ElapsedSQL(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryTop10ElapsedSQL",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryTop10RowsExamSQL(Map<String, String> params)
	 */
	@Override
	public List<Top10SQL> queryTop10RowsExamSQL(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryTop10RowsExamSQL",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryTop10TableIOWait(Map<String, String> params)
	 */
	@Override
	public List<Top10ObjectIOWait> queryTop10TableIOWait(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryTop10TableIOWait",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryTop10IndexIOWait(Map<String, String> params)
	 */
	@Override
	public List<Top10ObjectIOWait> queryTop10IndexIOWait(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryTop10IndexIOWait",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryTop10IndexRowsRead(Map<String, String> params)
	 */
	@Override
	public List<ISIndexStatistics> queryTop10IndexRowsRead(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryTop10IndexRowsRead",params); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.OverviewDAO#queryTop10TableRowsRead(Map<String, String> params)
	 */
	@Override
	public List<ISTableStatistics> queryTop10TableRowsRead(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryTop10TableRowsRead",params); 
	}

	@Override
	public List<SlaveStatus> querySlaveStatus(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "querySlaveStatus",params); 
	}

	@Override
	public List<OsInfo> queryOsInfo(Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryOsInfo",params); 
	}

	@Override
	public ISGlobalStatus getGlobalStatus(String appname, String snapId,
			String variableName) {
		Map<String,String> params = new HashMap<String,String>();
		params.put("variableName", variableName);
		params.put("appname", appname);
		params.put("snapId", snapId);
		return sqlSession.selectOne(DAO_NAMESPACE + "getGlobalStatus",params); 
	}

	@Override
	public List<PSEventsWaitsSummaryGlobalByEventName> queryTop10EventDetail(
			Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryTop10EventDetail",params); 
	}

	@Override
	public List<PSEventsWaitsSummaryGlobalByEventName> queryTop10EventSummary(
			Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryTop10EventSummary",params); 
	}

	@Override
	public List<TopSlowQueryLogDigest> queryTop10SlowQueryLogDigest(
			Map<String, String> params) {
		return sqlSession.selectList(DAO_NAMESPACE + "queryTop10SlowQueryLogDigest",params);
	}
}
