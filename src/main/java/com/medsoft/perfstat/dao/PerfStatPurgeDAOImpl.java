/**   
* @Title: MetadataDAOImpl.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:22:00 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.medsoft.perfstat.pojo.AppSnap;

/**
 * @author zjhua
 *
 */
@Service
public class PerfStatPurgeDAOImpl implements PerfStatPurgeDAO {

	private static final String DAO_NAMESPACE = "com.medsoft.perfstat.mybatis.Purge.";
	
	@Autowired
	@Qualifier("sqlSession")
	private SqlSession sqlSession;

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatPurgeDAO#deleteAppSnaps(String lastWeekDate)
	 */
	@Override
	public void deleteAppSnaps(String lastWeekDate) {
		Map<String,String> param = new HashMap<String,String>();
		param.put("deleteDate", lastWeekDate);
		sqlSession.delete(DAO_NAMESPACE + "deleteAppSnaps",param);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatPurgeDAO#deleteISGlobalStatus(String lastWeekDate)
	 */
	@Override
	public void deleteISGlobalStatus(String lastWeekDate) {
		Map<String,String> param = new HashMap<String,String>();
		param.put("deleteDate", lastWeekDate);
		sqlSession.delete(DAO_NAMESPACE + "deleteISGlobalStatus",param);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatPurgeDAO#deleteISGlobalVariables(String lastWeekDate)
	 */
	@Override
	public void deleteISGlobalVariables(String lastWeekDate) {
		Map<String,String> param = new HashMap<String,String>();
		param.put("deleteDate", lastWeekDate);
		sqlSession.delete(DAO_NAMESPACE + "deleteISGlobalVariables",param);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatPurgeDAO#deletePSTableIOWaitsSummaryByTable(String lastWeekDate)
	 */
	@Override
	public void deletePSTableIOWaitsSummaryByTable(String lastWeekDate) {
		Map<String,String> param = new HashMap<String,String>();
		param.put("deleteDate", lastWeekDate);
		sqlSession.delete(DAO_NAMESPACE + "deletePSTableIOWaitsSummaryByTable",param);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatPurgeDAO#deletePSTableLockWaitsSummaryByTable(String lastWeekDate)
	 */
	@Override
	public void deletePSTableLockWaitsSummaryByTable(String lastWeekDate) {
		Map<String,String> param = new HashMap<String,String>();
		param.put("deleteDate", lastWeekDate);
		sqlSession.delete(DAO_NAMESPACE + "deletePSTableLockWaitsSummaryByTable",param);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatPurgeDAO#deletePSTableIOWaitsSummaryByIndexUsage(String lastWeekDate)
	 */
	@Override
	public void deletePSTableIOWaitsSummaryByIndexUsage(String lastWeekDate) {
		Map<String,String> param = new HashMap<String,String>();
		param.put("deleteDate", lastWeekDate);
		sqlSession.delete(DAO_NAMESPACE + "deletePSTableIOWaitsSummaryByIndexUsage",param);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatPurgeDAO#deletePSEventsStatementsSummaryByDigest(String lastWeekDate)
	 */
	@Override
	public void deletePSEventsStatementsSummaryByDigest(String lastWeekDate) {
		Map<String,String> param = new HashMap<String,String>();
		param.put("deleteDate", lastWeekDate);
		sqlSession.delete(DAO_NAMESPACE + "deletePSEventsStatementsSummaryByDigest",param);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatPurgeDAO#deleteISIndexStatistics(String lastWeekDate)
	 */
	@Override
	public void deleteISIndexStatistics(String lastWeekDate) {
		Map<String,String> param = new HashMap<String,String>();
		param.put("deleteDate", lastWeekDate);
		sqlSession.delete(DAO_NAMESPACE + "deleteISIndexStatistics",param);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatPurgeDAO#deletePSEventsWaitsSummaryGlobalByEventName(String lastWeekDate)
	 */
	@Override
	public void deletePSEventsWaitsSummaryGlobalByEventName(String lastWeekDate) {
		Map<String,String> param = new HashMap<String,String>();
		param.put("deleteDate", lastWeekDate);
		sqlSession.delete(DAO_NAMESPACE + "deletePSEventsWaitsSummaryGlobalByEventName",param);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatPurgeDAO#deleteISTableStatistics(String lastWeekDate)
	 */
	@Override
	public void deleteISTableStatistics(String lastWeekDate) {
		Map<String,String> param = new HashMap<String,String>();
		param.put("deleteDate", lastWeekDate);
		sqlSession.delete(DAO_NAMESPACE + "deleteISTableStatistics",param);
	}

	@Override
	public String getLastWeekDate() {
		Map<String,String> param = new HashMap<String,String>();
		return sqlSession.selectOne(DAO_NAMESPACE + "getLastWeekDate",param);
	}

	@Override
	public void deleteSlowQuerySummary(String lastWeekDate) {
		Map<String,String> param = new HashMap<String,String>();
		param.put("deleteDate", lastWeekDate);
		sqlSession.delete(DAO_NAMESPACE + "deleteSlowQuerySummary",param);
	}

	@Override
	public void deleteAppSnaps(AppSnap appSnap) {
		sqlSession.delete(DAO_NAMESPACE + "deleteAppSnapsByAppSnap",appSnap);
	}

	@Override
	public void deletePSEventsWaitsSummaryGlobalByEventName(AppSnap appSnap) {
		sqlSession.delete(DAO_NAMESPACE + "deletePSEventsWaitsSummaryGlobalByEventNameByAppSnap",appSnap);
	}

	@Override
	public void deletePSTableIOWaitsSummaryByTable(AppSnap appSnap) {
		sqlSession.delete(DAO_NAMESPACE + "deletePSTableIOWaitsSummaryByTableByAppSnap",appSnap);
	}

	@Override
	public void deleteSlowQuerySummary(AppSnap appSnap) {
		sqlSession.delete(DAO_NAMESPACE + "deleteSlowQuerySummaryByAppSnap",appSnap);
	}

	@Override
	public void deletePSEventsStatementsSummaryByDigest(AppSnap appSnap) {
		sqlSession.delete(DAO_NAMESPACE + "deletePSEventsStatementsSummaryByDigestByAppSnap",appSnap);
	}

	@Override
	public void deleteISGlobalVariables(AppSnap appSnap) {
		sqlSession.delete(DAO_NAMESPACE + "deleteISGlobalVariablesByAppSnap",appSnap);
	}

	@Override
	public void deleteISGlobalStatus(AppSnap appSnap) {
		sqlSession.delete(DAO_NAMESPACE + "deleteISGlobalStatusByAppSnap",appSnap);
	}

	@Override
	public void deleteISIndexStatistics(AppSnap appSnap) {
		sqlSession.delete(DAO_NAMESPACE + "deleteISIndexStatisticsByAppSnap",appSnap);
	}

	@Override
	public void deleteISTableStatistics(AppSnap appSnap) {
		sqlSession.delete(DAO_NAMESPACE + "deleteISTableStatisticsByAppSnap",appSnap);
	}

	@Override
	public void deletePSTableIOWaitsSummaryByIndexUsage(AppSnap appSnap) {
		sqlSession.delete(DAO_NAMESPACE + "deletePSTableIOWaitsSummaryByIndexUsageByAppSnap",appSnap);
	}

	@Override
	public void deletePSTableLockWaitsSummaryByTable(AppSnap appSnap) {
		sqlSession.delete(DAO_NAMESPACE + "deletePSTableLockWaitsSummaryByTableByAppSnap",appSnap);
	}
}
