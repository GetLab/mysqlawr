/**   
* @Title: MetadataDAOImpl.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:22:00 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.medsoft.perfstat.pojo.AppSnap;
import com.medsoft.perfstat.pojo.OsInfo;

/**
 * @author zjhua
 *
 */
@Service
public class PerfStatInsertDAOImpl implements PerfStatInsertDAO {

	private static final String DAO_NAMESPACE = "com.medsoft.perfstat.mybatis.Collect.";
	
	@Autowired
	@Qualifier("sqlSession")
	private SqlSession sqlSession;

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatInsertDAO#insertAppSnaps()
	 */
	@Override
	public void insertAppSnaps(AppSnap appSnap) {
		sqlSession.insert(DAO_NAMESPACE + "insertAppSnaps",appSnap);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatInsertDAO#insertISGlobalStatus()
	 */
	@Override
	public void insertISGlobalStatus(String params) {
		Map<String,String> values = new HashMap<String,String>();
		values.put("ISGlobalStatus", params);
		sqlSession.insert(DAO_NAMESPACE + "insertISGlobalStatus",values);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatInsertDAO#insertISGlobalVariables()
	 */
	@Override
	public void insertISGlobalVariables(String params) {
		Map<String,String> values = new HashMap<String,String>();
		values.put("ISGlobalVariables", params);
		sqlSession.insert(DAO_NAMESPACE + "insertISGlobalVariables",values);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatInsertDAO#insertPSTableIOWaitsSummaryByTable()
	 */
	@Override
	public void insertPSTableIOWaitsSummaryByTable(String params) {
		Map<String,String> values = new HashMap<String,String>();
		values.put("PSTableIOWaitsSummaryByTables", params);
		sqlSession.insert(DAO_NAMESPACE + "insertPSTableIOWaitsSummaryByTable",values);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatInsertDAO#insertPSTableLockWaitsSummaryByTable()
	 */
	@Override
	public void insertPSTableLockWaitsSummaryByTable(String params) {
		Map<String,String> values = new HashMap<String,String>();
		values.put("PSTableLockWaitsSummaryByTables", params);
		sqlSession.insert(DAO_NAMESPACE + "insertPSTableLockWaitsSummaryByTable",values);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatInsertDAO#insertPSTableIOWaitsSummaryByIndexUsage()
	 */
	@Override
	public void insertPSTableIOWaitsSummaryByIndexUsage(String params) {
		Map<String,String> values = new HashMap<String,String>();
		values.put("PSTableIOWaitsSummaryByIndexUsages", params);
		sqlSession.insert(DAO_NAMESPACE + "insertPSTableIOWaitsSummaryByIndexUsage",values);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatInsertDAO#insertPSEventsStatementsSummaryByDigest()
	 */
	@Override
	public void insertPSEventsStatementsSummaryByDigest(String params) {
		Map<String,String> values = new HashMap<String,String>();
		values.put("PSEventsStatementsSummaryByDigests", params);
		sqlSession.insert(DAO_NAMESPACE + "insertPSEventsStatementsSummaryByDigest",values);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatInsertDAO#insertISIndexStatistics()
	 */
	@Override
	public void insertISIndexStatistics(String params) {
		Map<String,String> values = new HashMap<String,String>();
		values.put("ISIndexStatistics", params);
		sqlSession.insert(DAO_NAMESPACE + "insertISIndexStatistics",values);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatInsertDAO#insertPSEventsWaitsSummaryGlobalByEventName()
	 */
	@Override
	public void insertPSEventsWaitsSummaryGlobalByEventName(String params) {
		Map<String,String> values = new HashMap<String,String>();
		values.put("PSEventsWaitsSummaryGlobalByEventNames", params);
		sqlSession.insert(DAO_NAMESPACE + "insertPSEventsWaitsSummaryGlobalByEventName",values);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatInsertDAO#insertISTableStatistics()
	 */
	@Override
	public void insertISTableStatistics(String params) {
		Map<String,String> values = new HashMap<String,String>();
		values.put("ISTableStatistics", params);
		sqlSession.insert(DAO_NAMESPACE + "insertISTableStatistics",values);
	}

	@Override
	public void insertInnodbBufferPoolStats(String params) {
		Map<String,String> values = new HashMap<String,String>();
		values.put("ISInnodbBufferPoolStats", params);
		sqlSession.insert(DAO_NAMESPACE + "insertInnodbBufferPoolStats",values);
	}

	@Override
	public void insertSlaveStatus(String params) {
		Map<String,String> values = new HashMap<String,String>();
		values.put("ISSlaveStatus", params);
		sqlSession.insert(DAO_NAMESPACE + "insertSlaveStatus",values);
	}

	@Override
	public void insertOsInfo(OsInfo osInfo) {
		sqlSession.insert(DAO_NAMESPACE + "insertOsInfo",osInfo);
	}

	@Override
	public void insertSlowQuerySummary(String params) {
		Map<String,String> values = new HashMap<String,String>();
		values.put("SlowQuerys", params);
		sqlSession.insert(DAO_NAMESPACE + "insertSlowQuerySummary",values);
	}
}
