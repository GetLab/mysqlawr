/**   
* @Title: MetadataUpdateDAO.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:10:29 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import com.medsoft.perfstat.pojo.Proc;

/**
 * @author zjhua
 *
 */
public interface ProcDepsSrcDAO {

	String getProcBody(String db, String name);

	Proc getProc(String db, String name);
	
}
