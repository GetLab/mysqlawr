/**   
* @Title: MetadataUpdateDAO.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:10:29 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import java.util.List;

import com.medsoft.perfstat.pojo.AppSnap;
import com.medsoft.perfstat.pojo.Apps;

/**
 * @author zjhua
 *
 */
public interface MetadataDAO {
	List<String> querySnapId();
	
	List<String> queryAppname();
	
	List<String> queryHostname();
	
	List<Apps> queryApps(String appname,int snapId);
	
	Integer getNextAppSnap(Apps apps);

	/**
	 * @return
	 */
	List<AppSnap> queryAppSnaps(String appname);

	void addApps(Apps apps);
}
