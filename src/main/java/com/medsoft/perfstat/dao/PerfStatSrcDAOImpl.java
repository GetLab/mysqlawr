/**   
* @Title: MetadataDAOImpl.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:22:00 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.medsoft.perfstat.pojo.ISGlobalStatus;
import com.medsoft.perfstat.pojo.ISGlobalVariable;
import com.medsoft.perfstat.pojo.ISIndexStatistics;
import com.medsoft.perfstat.pojo.ISTableStatistics;
import com.medsoft.perfstat.pojo.InnodbBufferPoolStats;
import com.medsoft.perfstat.pojo.PSEventsStatementsSummaryByDigest;
import com.medsoft.perfstat.pojo.PSEventsWaitsSummaryGlobalByEventName;
import com.medsoft.perfstat.pojo.PSTableIOWaitsSummaryByIndexUsage;
import com.medsoft.perfstat.pojo.PSTableIOWaitsSummaryByTable;
import com.medsoft.perfstat.pojo.PSTableLockWaitsSummaryByTable;
import com.medsoft.perfstat.pojo.SlaveStatus;

/**
 * @author zjhua
 *
 */
@Service
public class PerfStatSrcDAOImpl implements PerfStatSrcDAO {

	private static final String DAO_NAMESPACE = "com.medsoft.perfstat.mybatis.SrcCollect.";
	
	@Autowired
	@Qualifier("sqlSessionTarget")
	private SqlSession sqlSessionTarget;

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatSrcDAO#queryISGlobalStatus()
	 */
	@Override
	public List<ISGlobalStatus> queryISGlobalStatus() {
		try {
			return sqlSessionTarget.selectList(DAO_NAMESPACE + "selectISGlobalStatus");
		} finally {
			
		}
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatSrcDAO#queryISGlobalVariables()
	 */
	@Override
	public List<ISGlobalVariable> queryISGlobalVariables() {
		return sqlSessionTarget.selectList(DAO_NAMESPACE + "selectISGlobalVariables"); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatSrcDAO#queryPSTableIOWaitsSummaryByTable()
	 */
	@Override
	public List<PSTableIOWaitsSummaryByTable> queryPSTableIOWaitsSummaryByTable() {
		List<PSTableIOWaitsSummaryByTable> list = sqlSessionTarget.selectList(DAO_NAMESPACE + "selectPSTableIOWaitsSummaryByTable");
		//sqlSessionTarget.update(DAO_NAMESPACE + "setSQL_LOG_BIN_OFF");
		//sqlSessionTarget.delete(DAO_NAMESPACE + "truncatePSTableIOWaitsSummaryByTable");
		//sqlSessionTarget.update(DAO_NAMESPACE + "setSQL_LOG_BIN_ON");
		return list;
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatSrcDAO#queryPSTableLockWaitsSummaryByTable()
	 */
	@Override
	public List<PSTableLockWaitsSummaryByTable> queryPSTableLockWaitsSummaryByTable() {
		List<PSTableLockWaitsSummaryByTable> list = sqlSessionTarget.selectList(DAO_NAMESPACE + "selectPSTableLockWaitsSummaryByTable"); 
		//sqlSessionTarget.update(DAO_NAMESPACE + "setSQL_LOG_BIN_OFF");
		//sqlSessionTarget.delete(DAO_NAMESPACE + "truncatePSTableLockWaitsSummaryByTable");
		//sqlSessionTarget.update(DAO_NAMESPACE + "setSQL_LOG_BIN_ON");
		return list;
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatSrcDAO#queryPSTableIOWaitsSummaryByIndexUsage()
	 */
	@Override
	public List<PSTableIOWaitsSummaryByIndexUsage> queryPSTableIOWaitsSummaryByIndexUsage() {
		List<PSTableIOWaitsSummaryByIndexUsage> list = sqlSessionTarget.selectList(DAO_NAMESPACE + "selectPSTableIOWaitsSummaryByIndexUsage");
		//sqlSessionTarget.update(DAO_NAMESPACE + "setSQL_LOG_BIN_OFF");
		//sqlSessionTarget.delete(DAO_NAMESPACE + "truncatePSTableIOWaitsSummaryByIndexUsage");
		//sqlSessionTarget.update(DAO_NAMESPACE + "setSQL_LOG_BIN_ON");
		return list; 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatSrcDAO#queryPSEventsStatementsSummaryByDigest()
	 */
	@Override
	public List<PSEventsStatementsSummaryByDigest> queryPSEventsStatementsSummaryByDigest() {
		List<PSEventsStatementsSummaryByDigest> list = sqlSessionTarget.selectList(DAO_NAMESPACE + "selectPSEventsStatementsSummaryByDigest");
		//sqlSessionTarget.update(DAO_NAMESPACE + "setSQL_LOG_BIN_OFF");
		//sqlSessionTarget.delete(DAO_NAMESPACE + "truncatePSEventsStatementsSummaryByDigest");
		//sqlSessionTarget.update(DAO_NAMESPACE + "setSQL_LOG_BIN_ON");
		return list;
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatSrcDAO#queryISIndexStatistics()
	 */
	@Override
	public List<ISIndexStatistics> queryISIndexStatistics() {
		try {
			List<ISIndexStatistics> list = sqlSessionTarget.selectList(DAO_NAMESPACE + "selectISIndexStatistics"); 
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<ISIndexStatistics>();
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatSrcDAO#queryPSEventsWaitsSummaryGlobalByEventName()
	 */
	@Override
	public List<PSEventsWaitsSummaryGlobalByEventName> queryPSEventsWaitsSummaryGlobalByEventName() {
		List<PSEventsWaitsSummaryGlobalByEventName> list = sqlSessionTarget.selectList(DAO_NAMESPACE + "selectPSEventsWaitsSummaryGlobalByEventName");
		//sqlSessionTarget.update(DAO_NAMESPACE + "setSQL_LOG_BIN_OFF");
		//sqlSessionTarget.delete(DAO_NAMESPACE + "truncatePSEventsWaitsSummaryGlobalByEventName");
		//sqlSessionTarget.update(DAO_NAMESPACE + "setSQL_LOG_BIN_ON");
		return list;
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatSrcDAO#queryISTableStatistics()
	 */
	@Override
	public List<ISTableStatistics> queryISTableStatistics() {
		try {
			List<ISTableStatistics> list = sqlSessionTarget.selectList(DAO_NAMESPACE + "selectISTableStatistics");  
			return list;
		} catch (Exception e) {
			System.out.println("非percona/mariadb分支或userstat没有开启！");
		}
		return new ArrayList<ISTableStatistics>();
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatSrcDAO#queryISGlobalStatus57()
	 */
	@Override
	public List<ISGlobalStatus> queryISGlobalStatus57() {
		return sqlSessionTarget.selectList(DAO_NAMESPACE + "selectISGlobalStatus57"); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatSrcDAO#queryISGlobalVariables57()
	 */
	@Override
	public List<ISGlobalVariable> queryISGlobalVariables57() {
		return sqlSessionTarget.selectList(DAO_NAMESPACE + "selectISGlobalVariables57");
	}

	@Override
	public List<InnodbBufferPoolStats> queryInnodbBufferPoolStats() {
		return sqlSessionTarget.selectList(DAO_NAMESPACE + "selectInnodbBufferPoolStats");
	}

	@Override
	public List<SlaveStatus> querySlaveStatus() {
		return sqlSessionTarget.selectList(DAO_NAMESPACE + "selectSlaveStatus");
	}

	@Override
	public void disableSlowLog() {
		sqlSessionTarget.update(DAO_NAMESPACE + "disableSlowLog");
	}

	@Override
	public void enableSlowLog() {
		sqlSessionTarget.update(DAO_NAMESPACE + "enableSlowLog");
	}

	@Override
	public String getSlowFileName() {
		return sqlSessionTarget.selectOne(DAO_NAMESPACE + "getSlowFileName");
	}
	
	@Override
	public String getSlowFileName57() {
		return sqlSessionTarget.selectOne(DAO_NAMESPACE + "getSlowFileName57");
	}
}
