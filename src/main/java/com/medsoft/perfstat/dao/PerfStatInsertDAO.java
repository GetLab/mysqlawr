/**   
* @Title: MetadataUpdateDAO.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:10:29 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import com.medsoft.perfstat.pojo.AppSnap;
import com.medsoft.perfstat.pojo.OsInfo;

/**
 * @author zjhua
 *
 */
public interface PerfStatInsertDAO {
	void insertAppSnaps(AppSnap appSnap);
	void insertISGlobalStatus(String values);
	void insertISGlobalVariables(String values);
	void insertPSTableIOWaitsSummaryByTable(String values);
	void insertPSTableLockWaitsSummaryByTable(String values);
	void insertPSTableIOWaitsSummaryByIndexUsage(String values);
	void insertPSEventsStatementsSummaryByDigest(String values);
	void insertISIndexStatistics(String values);
	void insertPSEventsWaitsSummaryGlobalByEventName(String values);
	void insertISTableStatistics(String values);
	
	void insertInnodbBufferPoolStats(String values);
	void insertSlaveStatus(String values);
	void insertOsInfo(OsInfo osInfo);
	void insertSlowQuerySummary(String values);
}
