/**   
* @Title: MetadataDAOImpl.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:22:00 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.medsoft.perfstat.pojo.AppSnap;
import com.medsoft.perfstat.pojo.Apps;

/**
 * @author zjhua
 *
 */
@Service
public class MetadataDAOImpl implements MetadataDAO {

	private static final String DAO_NAMESPACE = "com.medsoft.perfstat.mybatis.Metadata.";
	
	@Autowired
	@Qualifier("sqlSession")
	private SqlSession sqlSession;
	
	/* (non-Javadoc)
	 * @see com.medsoft.logpool.core.MetadataDAO#queryLogLevel()
	 */
	@Override
	public List<String> querySnapId() {
		return sqlSession.selectList(DAO_NAMESPACE + "queryDistinctSnapId"); 
	}

	/* (non-Javadoc)
	 * @see com.medsoft.logpool.core.MetadataDAO#queryAppname()
	 */
	@Override
	public List<String> queryAppname() {
		return sqlSession.selectList(DAO_NAMESPACE + "queryDistinctAppname");
	}

	/* (non-Javadoc)
	 * @see com.medsoft.logpool.core.MetadataDAO#queryHostname()
	 */
	@Override
	public List<String> queryHostname() {
		return sqlSession.selectList(DAO_NAMESPACE + "queryDistinctHostname");
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.MetadataDAO#queryApps()
	 */
	@Override
	public List<Apps> queryApps(String appname,int snapInterval) {
		Map<String,String> params = new HashMap<String,String>();
		params.put("appname", appname);
		params.put("snapInterval", String.valueOf(snapInterval));
		return sqlSession.selectList(DAO_NAMESPACE + "queryApps",params);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.MetadataDAO#getNextAppSnap(com.medsoft.perfstat.pojo.Apps)
	 */
	@Override
	public Integer getNextAppSnap(Apps apps) {
		return sqlSession.selectOne(DAO_NAMESPACE + "getNextAppSnap",apps);
	}

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.MetadataDAO#queryAppSnaps()
	 */
	@Override
	public List<AppSnap> queryAppSnaps(String appname) {
		Map<String,String> params = new HashMap<String,String>();
		params.put("appname", appname);
		return sqlSession.selectList(DAO_NAMESPACE + "queryAppSnaps",params);
	}

	@Override
	public void addApps(Apps apps) {
		sqlSession.insert(DAO_NAMESPACE + "insertApps",apps);
	}
}
