/**   
* @Title: MetadataDAOImpl.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:22:00 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.medsoft.perfstat.pojo.Proc;

/**
 * @author zjhua
 *
 */
@Service
public class ProcDepsSrcDAOImpl implements ProcDepsSrcDAO {

	private static final String DAO_NAMESPACE = "com.medsoft.perfstat.mybatis.ProcDeps.";
	
	@Autowired
	@Qualifier("sqlSessionTarget")
	private SqlSession sqlSessionTarget;

	/* (non-Javadoc)
	 * @see com.medsoft.perfstat.dao.PerfStatSrcDAO#queryISGlobalStatus()
	 */
	@Override
	public String getProcBody(String db,String name) {
		try {
			Map<String,String> param = new HashMap<String,String>();
			param.put("db", db);
			param.put("name", name);
			return sqlSessionTarget.selectOne(DAO_NAMESPACE + "getProcBody",param);
		} finally {
			
		}
	}
	
	@Override
	public Proc getProc(String db,String name) {
		try {
			Map<String,String> param = new HashMap<String,String>();
			param.put("db", db);
			param.put("name", name);
			return sqlSessionTarget.selectOne(DAO_NAMESPACE + "getProc",param);
		} finally {
			
		}
	}
}
