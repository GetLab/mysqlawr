/**   
* @Title: GlobalStatusChange.java 
* @Package com.medsoft.perfstat.dao 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月16日 下午7:54:09 
* @version V1.0   
*/ 
package com.medsoft.perfstat.dao;

import java.util.List;
import java.util.Map;

import com.medsoft.perfstat.pojo.ISGlobalVariableChange;

/**
 * @author zjhua
 *
 */
public interface GlobalVariablesChangeDAO {
	List<ISGlobalVariableChange> queryGlobalVariablesChange(Map<String,String> param);
}
