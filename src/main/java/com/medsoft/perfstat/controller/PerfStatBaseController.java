/**   
 * @Title: LogpoolBaseController.java 
 * @Package com.medsoft.logpool.action 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author zjhua@hundsun.com   
 * @date 2016年7月12日 上午9:53:38 
 * @version V1.0   
 */
package com.medsoft.perfstat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.view.velocity.VelocityConfigurer;

import com.cyl.kernel.BaseController;
import com.cyl.kernel.util.DateUtil;
import com.medsoft.perfstat.pojo.AppSnapReq;
import com.medsoft.perfstat.service.MetadataContainer;

/**
 * @author zjhua
 *
 */
public class PerfStatBaseController extends BaseController {
	@Autowired
	private VelocityConfigurer velocityConfigurer;

	@ModelAttribute
	public void init(Model model) {
		model.addAttribute(MetadataContainer.APP_NAME,
				MetadataContainer.getAppname());
		model.addAttribute(MetadataContainer.HOST_NAME,
				MetadataContainer.getHostname());
		model.addAttribute(MetadataContainer.APP_SNAPS,
				MetadataContainer.getAppSnaps());
		model.addAttribute(MetadataContainer.SNAP_ID,
				MetadataContainer.getSnapId());
	}

	public VelocityConfigurer getVelocityConfigurer() {
		return velocityConfigurer;
	}

	/**
	 * @param req
	 */
	public void processReq(AppSnapReq req) {
		String beginSnapId = req.getBeginSnapId().substring(0,
				req.getBeginSnapId().indexOf("->"));
		String beginSnapTime = req.getBeginSnapId().substring(
				req.getBeginSnapId().indexOf("->") + 2);
		String endSnapId = req.getEndSnapId().substring(0,
				req.getEndSnapId().indexOf("->"));
		String endSnapTime = req.getEndSnapId().substring(
				req.getEndSnapId().indexOf("->") + 2);
		req.setBeginSnapId(beginSnapId);
		req.setBeginSnapTime(beginSnapTime);
		req.setEndSnapId(endSnapId);
		req.setEndSnapTime(endSnapTime);
		req.setElapsed(DateUtil.s2DDHHMISS(DateUtil.date2Timestamp(endSnapTime,
				DateUtil.MySQLDateFmt)
				- DateUtil.date2Timestamp(beginSnapTime, DateUtil.MySQLDateFmt)));
	}
}
