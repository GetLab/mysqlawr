package com.medsoft.perfstat.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cyl.kernel.BaseController;
import com.medsoft.perfstat.service.MetadataService;
import com.medsoft.perfstat.service.OverviewService;
import com.medsoft.perfstat.service.ProcDepsService;

/**
 * Created with IntelliJ IDEA. User: zjhua Date: 2015-01-02 Time: 23:56 Mail:
 * zjhua@hundsun.com Comment: 功能描述 Modifiy History: 修改历史 To change this template
 * use File | Settings | File Templates.
 */
@Controller
public class ProcDepsAction extends BaseController {
	@Autowired
	private ProcDepsService procDepsService;
	
	@Autowired
	private MetadataService metadataService;
	
	@RequestMapping(value = "/proc-deps.html",method = RequestMethod.GET)
	public void init(Model model,HttpServletRequest request) {
		model.addAttribute("hosts",metadataService.queryHosts());
	}

	@RequestMapping(value = "/proc-deps.html", method = RequestMethod.POST)
	public void queryAppSnaps(@RequestParam String appname,
			@RequestParam String db,
			@RequestParam String name,
							HttpServletRequest request,Model model) {
		model.addAttribute("hosts",metadataService.queryHosts());
		model.addAttribute("deps", procDepsService.getDeps(appname, db, name));
	}
}
