package com.medsoft.perfstat.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cyl.kernel.BaseController;
import com.cyl.kernel.DBContextHolder;
import com.cyl.kernel.InPacket;
import com.cyl.kernel.OutPacket;
import com.medsoft.perfstat.pojo.Apps;
import com.medsoft.perfstat.service.MetadataContainer;
import com.medsoft.perfstat.service.MetadataService;
import com.medsoft.perfstat.service.StatusChangeService;

/**
 * Created with IntelliJ IDEA. User: zjhua Date: 2015-01-02 Time: 23:56 Mail:
 * zjhua@hundsun.com Comment: 功能描述 Modifiy History: 修改历史 To change this template
 * use File | Settings | File Templates.
 */
@Controller
public class HostsAction extends BaseController {
	@Autowired
	private MetadataService metadataService;
	
	@RequestMapping(value = "/hosts.html")
	public void init(Model model,HttpServletRequest request) {
		model.addAttribute("hosts",metadataService.queryHosts());
	}
	
	@RequestMapping(value = "/hosts/add.html", method = RequestMethod.GET)
	public void addHostInit(HttpServletRequest request) {
	}

	@RequestMapping(value = "/hosts/add.html", method = RequestMethod.POST)
	public String addHost(Apps apps,HttpServletRequest request,Model model) {
		if(metadataService.addApps(apps,request)) {
			model.addAttribute("hosts",metadataService.queryHosts());
			return "redirect:/hosts.html";
		}
		model.addAttribute("errorCode","-1").addAttribute("errorInfo", "连接到" + apps.toString() + "出错，具体详见系统日志！");
		return "/error";
	}
}
