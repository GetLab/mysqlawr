/**   
 * @Title: VariableChangeAction.java 
 * @Package com.medsoft.perfstat.action 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author zjhua@hundsun.com   
 * @date 2016年7月13日 下午5:07:13 
 * @version V1.0   
 */
package com.medsoft.perfstat.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cyl.kernel.DBContextHolder;
import com.medsoft.perfstat.pojo.AppSnapReq;
import com.medsoft.perfstat.service.MetadataContainer;
import com.medsoft.perfstat.service.VariableChangeService;

/**
 * @author zjhua
 *
 */
@Controller
public class VariableChangeAction extends PerfStatBaseController {
	@Autowired
	private VariableChangeService variableChangeService;

	@RequestMapping(value = "/variables.html", method = RequestMethod.GET)
	public void index(Model model, HttpServletRequest request) {
		// NOP
	}

	@RequestMapping(value = "/variables.html", method = RequestMethod.POST)
	public void queryVariables(AppSnapReq req, Model model) {
		super.processReq(req);
		Map<String, String> map = new BeanMap(req);
		model.addAttribute("globalVariables",
				variableChangeService.queryGlobalVariablesChange(map));
	}
}
