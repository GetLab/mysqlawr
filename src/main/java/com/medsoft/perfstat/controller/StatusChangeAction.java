/**   
 * @Title: VariableChangeAction.java 
 * @Package com.medsoft.perfstat.action 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author zjhua@hundsun.com   
 * @date 2016年7月13日 下午5:07:13 
 * @version V1.0   
 */
package com.medsoft.perfstat.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cyl.kernel.DBContextHolder;
import com.medsoft.perfstat.pojo.AppSnapReq;
import com.medsoft.perfstat.service.MetadataContainer;
import com.medsoft.perfstat.service.StatusChangeService;

/**
 * @author zjhua
 *
 */
@Controller
public class StatusChangeAction extends PerfStatBaseController {
	@Autowired
	private StatusChangeService statusChangeService;

	@RequestMapping(value = "/status.html", method = RequestMethod.GET)
	public void index(Model model, HttpServletRequest request) {
		// NOP
	}

	@RequestMapping(value = "/status.html", method = RequestMethod.POST)
	public void queryStatus(AppSnapReq req, Model model) {
		super.processReq(req);
		Map<String, String> map = new BeanMap(req);
		model.addAttribute("globalStatus",
				statusChangeService.queryGlobalStatusChange(map));
	}
}
