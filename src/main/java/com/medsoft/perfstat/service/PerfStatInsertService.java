/**   
* @Title: MetadataUpdateService.java 
* @Package com.medsoft.logpool.core 
* @Description: (用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:15:32 
* @version V1.0   
*/ 
package com.medsoft.perfstat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medsoft.perfstat.dao.PerfStatInsertDAO;
import com.medsoft.perfstat.pojo.AppSnap;
import com.medsoft.perfstat.pojo.ISGlobalStatus;
import com.medsoft.perfstat.pojo.ISGlobalVariable;
import com.medsoft.perfstat.pojo.ISIndexStatistics;
import com.medsoft.perfstat.pojo.ISTableStatistics;
import com.medsoft.perfstat.pojo.InnodbBufferPoolStats;
import com.medsoft.perfstat.pojo.OsInfo;
import com.medsoft.perfstat.pojo.PSEventsStatementsSummaryByDigest;
import com.medsoft.perfstat.pojo.PSEventsWaitsSummaryGlobalByEventName;
import com.medsoft.perfstat.pojo.PSTableIOWaitsSummaryByIndexUsage;
import com.medsoft.perfstat.pojo.PSTableIOWaitsSummaryByTable;
import com.medsoft.perfstat.pojo.PSTableLockWaitsSummaryByTable;
import com.medsoft.perfstat.pojo.SlaveStatus;
import com.medsoft.perfstat.pojo.SlowQueryLogDigest;

/**
 * @author zjhua
 *
 */
@Service
public class PerfStatInsertService {
	@Autowired
	private PerfStatInsertDAO perfStatInsertDAO;
	
	@Transactional
	void insertAppSnaps(AppSnap appSnap) {
		perfStatInsertDAO.insertAppSnaps(appSnap);
	};
	
	@Transactional
	void insertISGlobalStatus(AppSnap appSnap,List<ISGlobalStatus> list) {
		String values = "";
		int i = 0;
		if(list == null || list.size() == 0) {
			return;
		}
		for(ISGlobalStatus status : list) {
			if(i>0) {
				values = values + ",";
			}
			values = values + "("
					+ "'" + appSnap.getHostname() + "',"
					+ "'" + appSnap.getAppname() + "',"
					+ "'" + appSnap.getLogTime() + "',"
					+ appSnap.getSnapId() + ","
					+ "'" + status.getVariableName().toUpperCase()  + "',"
					+ "'" + status.getVariableValue().replace("\\", "//").substring(0, Math.min(64, status.getVariableValue().length())) + "'" + ")";
			i++;
		}
		
		perfStatInsertDAO.insertISGlobalStatus(values);
	};
	
	@Transactional
	void insertISGlobalVariables(AppSnap appSnap,List<ISGlobalVariable> list) {
		String values = "";
		int i = 0;
		if(list == null || list.size() == 0) {
			return;
		}
		for(ISGlobalVariable variable : list) {
			if(i>0) {
				values = values + ",";
			}
			values = values + "(" 
					+ "'" + appSnap.getHostname() + "',"
					+ "'" + appSnap.getAppname() + "',"
					+ "'" + appSnap.getLogTime() + "',"
					+ appSnap.getSnapId() + ","
					+ "'" + variable.getVariableName().toUpperCase() + "',"
					+ "'" + variable.getVariableValue().replace("\\", "//").substring(0, Math.min(64, variable.getVariableValue().length())) + "'" + ")";
			i++;
		}
		perfStatInsertDAO.insertISGlobalVariables(values);
	};
	
	@Transactional
	void insertPSTableIOWaitsSummaryByTable(AppSnap appSnap,List<PSTableIOWaitsSummaryByTable> list) {
		String values = "";
		int i = 0;
		if(list == null || list.size() == 0) {
			return;
		}
		for(PSTableIOWaitsSummaryByTable iowait : list) {
			if(i>0) {
				values = values + ",";
			}
			values = values + "(" 
					+ "'" + appSnap.getHostname() + "',"
					+ "'" + appSnap.getAppname() + "',"
					+ "'" + appSnap.getLogTime() + "',"
					+ appSnap.getSnapId() + ","
					+ "'" + iowait.getObjectType() + "',"
					+ "'" + iowait.getObjectSchema() + "',"
					+ "'" + iowait.getObjectName() + "',"
					+ iowait.getCountStar() + ","
					+ iowait.getSumTimerWait() + ","
					+ iowait.getMinTimerWait() + ","
					+ iowait.getAvgTimerWait() + ","
					+ iowait.getMaxTimerWait() + ","
					+ iowait.getCountRead() + ","
					+ iowait.getSumTimerRead() + ","
					+ iowait.getMinTimerRead() + ","
					+ iowait.getAvgTimerRead() + ","
					+ iowait.getMaxTimerRead() + ","
					+ iowait.getCountWrite() + ","
					+ iowait.getSumTimerWrite() + ","
					+ iowait.getMinTimerWrite() + ","
					+ iowait.getAvgTimerWrite() + ","
					+ iowait.getMaxTimerWrite() + ")";
			i++;
		}
		perfStatInsertDAO.insertPSTableIOWaitsSummaryByTable(values);
	};
	
	@Transactional
	void insertPSTableLockWaitsSummaryByTable(AppSnap appSnap,List<PSTableLockWaitsSummaryByTable> list) {
		String values = "";
		int i = 0;
		if(list == null || list.size() == 0) {
			return;
		}
		for(PSTableLockWaitsSummaryByTable lockwait : list) {
			if(i>0) {
				values = values + ",";
			}
			values = values + "(" 
					+ "'" + appSnap.getHostname() + "',"
					+ "'" + appSnap.getAppname() + "',"
					+ "'" + appSnap.getLogTime() + "',"
					+ appSnap.getSnapId() + ","
					+ "'" + lockwait.getObjectType() + "',"
					+ "'" + lockwait.getObjectSchema() + "',"
					+ "'" + lockwait.getObjectName() + "',"
					+ lockwait.getCountStar() + ","
					+ lockwait.getSumTimerWait() + ","
					+ lockwait.getMinTimerWait() + ","
					+ lockwait.getAvgTimerWait() + ","
					+ lockwait.getMaxTimerWait() + ","
					+ lockwait.getCountRead() + ","
					+ lockwait.getSumTimerRead() + ","
					+ lockwait.getMinTimerRead() + ","
					+ lockwait.getAvgTimerRead() + ","
					+ lockwait.getMaxTimerRead() + ","
					+ lockwait.getCountWrite() + ","
					+ lockwait.getSumTimerWrite() + ","
					+ lockwait.getMinTimerWrite() + ","
					+ lockwait.getAvgTimerWrite() + ","
					+ lockwait.getMaxTimerWrite() + ")";
			i++;
		}
		perfStatInsertDAO.insertPSTableLockWaitsSummaryByTable(values);
	};
	
	@Transactional
	void insertPSTableIOWaitsSummaryByIndexUsage(AppSnap appSnap,List<PSTableIOWaitsSummaryByIndexUsage> list) {
		String values = "";
		int i = 0;
		if(list == null || list.size() == 0) {
			return;
		}
		for(PSTableIOWaitsSummaryByIndexUsage iowait : list) {
			if(i>0) {
				values = values + ",";
			}
			values = values + "(" 
					+ "'" + appSnap.getHostname() + "',"
					+ "'" + appSnap.getAppname()  + "',"
					+ "'" + appSnap.getLogTime()  + "',"
					+ appSnap.getSnapId() + ","
					+ "'" + iowait.getObjectType() + "',"
					+ "'" + iowait.getObjectSchema() + "',"
					+ "'" + iowait.getObjectName() + "',"
					+ "'" + iowait.getIndexName() + "',"
					+ iowait.getCountStar() + ","
					+ iowait.getSumTimerWait() + ","
					+ iowait.getMinTimerWait() + ","
					+ iowait.getAvgTimerWait() + ","
					+ iowait.getMaxTimerWait() + ","
					+ iowait.getCountRead() + ","
					+ iowait.getSumTimerRead() + ","
					+ iowait.getMinTimerRead() + ","
					+ iowait.getAvgTimerRead() + ","
					+ iowait.getMaxTimerRead() + ","
					+ iowait.getCountWrite() + ","
					+ iowait.getSumTimerWrite() + ","
					+ iowait.getMinTimerWrite() + ","
					+ iowait.getAvgTimerWrite() + ","
					+ iowait.getMaxTimerWrite() + ")";
			i++;
		}
		perfStatInsertDAO.insertPSTableIOWaitsSummaryByIndexUsage(values);
	};
	
	@Transactional
	void insertPSEventsStatementsSummaryByDigest(AppSnap appSnap,List<PSEventsStatementsSummaryByDigest> list) {
		String values = "";
		int i = 0;
		if(list == null || list.size() == 0) {
			return;
		}
		for(PSEventsStatementsSummaryByDigest digest : list) {
			if(i>0) {
				values = values + ",";
			}
			values = values + "(" 
					+ "'" + appSnap.getHostname() + "',"
					+ "'" + appSnap.getAppname() + "',"
					+ "'" + appSnap.getLogTime() + "',"
					+ appSnap.getSnapId() + ","
					+ "'" + digest.getSchemaName() + "',"
					+ "'" + digest.getDigest() + "',"
					+ "'" + digest.getDigestText() + "',"
					+ digest.getCountStar() + ","
					+ digest.getSumTimerWait() + ","
					+ digest.getMinTimerWait() + ","
					+ digest.getAvgTimerWait() + ","
					+ digest.getMaxTimerWait() + ","
					+ digest.getSumLockTime() + ","
					+ digest.getSumErrors() + ","
					+ digest.getSumWarnings() + ","
					+ digest.getSumRowsAffected() + ","
					+ digest.getSumRowsSent() + ","
					+ digest.getSumRowsExamined() + ","
					+ digest.getSumCreatedTmpDiskTables() + ","
					+ digest.getSumCreatedTmpTables() + ","
					+ digest.getSumSelectFullJoin() + ","
					+ digest.getSumSelectFullRangeJoin() + ","
					+ digest.getSumSelectRange() + ","
					+ digest.getSumSelectRangeCheck() + ","
					+ digest.getSumSelectScan() + ","
					+ digest.getSumSortMergePasses() + ","
					+ digest.getSumSortRange() + ","
					+ digest.getSumSortRows() + ","
					+ digest.getSumSortScan() + ","
					+ digest.getSumNoIndexUsed() + ","
					+ digest.getSumNoGoodIndexUsed() + ","
					+ "'" + digest.getFirstSeen() + "',"
					+ "'" + digest.getLastSeen() + "')";
			i++;
		}
		perfStatInsertDAO.insertPSEventsStatementsSummaryByDigest(values);
	};
	
	@Transactional
	void insertISIndexStatistics(AppSnap appSnap,List<ISIndexStatistics> list) {
		String values = "";
		int i = 0;
		if(list == null || list.size() == 0) {
			return;
		}
		for(ISIndexStatistics indexStat : list) {
			if(i>0) {
				values = values + ",";
			}
			values = values + "(" 
					+ "'" + appSnap.getHostname() + "',"
					+ "'" + appSnap.getAppname() + "',"
					+ "'" + appSnap.getLogTime() + "',"
					+ appSnap.getSnapId() + ","
					+ "'" + indexStat.getTableSchema() + "',"
					+ "'" + indexStat.getTableName() + "',"
					+ "'" + indexStat.getIndexName() + "',"
					+ indexStat.getRowsRead() + ")";
			i++;
		}
		perfStatInsertDAO.insertISIndexStatistics(values);
	};
	
	@Transactional
	void insertISTableStatistics(AppSnap appSnap,List<ISTableStatistics> list) {
		String values = "";
		int i = 0;
		if(list == null || list.size() == 0) {
			return;
		}
		for(ISTableStatistics tableStat : list) {
			if(i>0) {
				values = values + ",";
			}
			values = values + "(" 
					+ "'" + appSnap.getHostname()  + "',"
					+ "'" + appSnap.getAppname()  + "',"
					+ "'" + appSnap.getLogTime()  + "',"
					+ appSnap.getSnapId() + ","
					+ "'" + tableStat.getTableSchema() + "',"
					+ "'" + tableStat.getTableName() + "',"
					+ tableStat.getRowsRead() + ","
					+ tableStat.getRowsChanged() + ","
					+ tableStat.getRowsChangedXIndexes() + ")";
			i++;
		}
		perfStatInsertDAO.insertISTableStatistics(values);
	};
	
	@Transactional
	void insertPSEventsWaitsSummaryGlobalByEventName(AppSnap appSnap,List<PSEventsWaitsSummaryGlobalByEventName> list) {
		String values = "";
		int i = 0;
		if(list == null || list.size() == 0) {
			return;
		}
		for(PSEventsWaitsSummaryGlobalByEventName event : list) {
			if(i>0) {
				values = values + ",";
			}
			values = values + "(" 
					+ "'" + appSnap.getHostname()  + "',"
					+ "'" + appSnap.getAppname()  + "',"
					+ "'" + appSnap.getLogTime()  + "',"
					+ appSnap.getSnapId() + ","
					+ "'" + event.getEventName() + "',"
					+ event.getCountStar() + ","
					+ event.getSumTimerWait() + ","
					+ event.getMinTimerWait() + ","
					+ event.getAvgTimerWait() + ","
					+ event.getMaxTimerWait() + ")";
			i++;
		}
		perfStatInsertDAO.insertPSEventsWaitsSummaryGlobalByEventName(values);
	}

	@Transactional
	public void insertSlaveStatus(AppSnap appSnap, List<SlaveStatus> list) {
		String values = "";
		int i = 0;
		if(list == null || list.size() == 0) {
			return;
		}
		for(SlaveStatus slaveStatus : list) {
			if(i>0) {
				values = values + ",";
			}
			values = values + "(" 
					+ "'" + appSnap.getHostname()  + "',"
					+ "'" + appSnap.getAppname()  + "',"
					+ "'" + appSnap.getLogTime()  + "',"
					+ appSnap.getSnapId() + ","
					+ "'" + slaveStatus.getSlaveIoState() + "',"
					+ "'" + slaveStatus.getMasterHost() + "',"
					+ "'" + slaveStatus.getMasterUser() + "',"
					+ "'" + slaveStatus.getMasterPort() + "',"
					+ "'" + slaveStatus.getConnectRetry() + "',"
					+ "'" + slaveStatus.getMasterLogFile() + "',"
					+ "'" + slaveStatus.getReadMasterLogPos() + "',"
					+ "'" + slaveStatus.getRelayLogFile() + "',"
					+ "'" + slaveStatus.getRelayLogPos() + "',"
					+ "'" + slaveStatus.getRelayMasterLogFile() + "',"
					+ "'" + slaveStatus.getSlaveIoRunning() + "',"
					+ "'" + slaveStatus.getSlaveSqlRunning() + "',"
					+ "'" + slaveStatus.getReplicateDoDb() + "',"
					+ "'" + slaveStatus.getReplicateIgnoreDb() + "',"
					+ "'" + slaveStatus.getReplicateDoTable() + "',"
					+ "'" + slaveStatus.getReplicateIgnoreTable() + "',"
					+ "'" + slaveStatus.getReplicateWildDoTable() + "',"
					+ "'" + slaveStatus.getReplicateWildIgnoreTable() + "',"
					+ "'" + slaveStatus.getLastErrno() + "',"
					+ "'" + slaveStatus.getLastError() + "',"
					+ "'" + slaveStatus.getSkipCounter() + "',"
					+ "'" + slaveStatus.getExecMasterLogPos() + "',"
					+ "'" + slaveStatus.getRelayLogSpace() + "',"
					+ "'" + slaveStatus.getUntilCondition() + "',"
					+ "'" + slaveStatus.getUntilLogFile() + "',"
					+ "'" + slaveStatus.getUntilLogPos() + "',"
					+ "'" + slaveStatus.getSecondsBehindMaster() + "',"
					+ "'" + slaveStatus.getLastIoErrno() + "',"
					+ "'" + slaveStatus.getLastIoError().replaceAll("'", " ") + "'," 
					+ "'" + slaveStatus.getLastSqlErrno() + "',"
					+ "'" + slaveStatus.getLastSqlError() + "',"
					+ "'" + slaveStatus.getReplicateIgnoreServerIds() + "',"
					+ "'" + slaveStatus.getMasterServerId() + "',"
					+ "'" + slaveStatus.getMasterUuid() + "',"
					+ "'" + slaveStatus.getMasterInfoFile() + "',"
					+ "'" + slaveStatus.getSqlDelay() + "',"
					+ "'" + slaveStatus.getSqlRemainingDelay() + "',"
					+ "'" + slaveStatus.getSlaveSqlRunningState() + "',"
					+ "'" + slaveStatus.getMasterRetryCount() + "',"
					+ "'" + slaveStatus.getMasterBind() + "',"
					+ "'" + slaveStatus.getLastIoErrorTimestamp() + "',"
					+ "'" + slaveStatus.getLastSqlErrorTimestamp() + "',"
					+ "'" + slaveStatus.getRetrievedGtidSet() + "',"
					+ "'" + slaveStatus.getExecutedGtidSet() + "',"
					+ "'" + slaveStatus.getAutoPosition() + "',"
					+ "'" + slaveStatus.getReplicateRewriteDb() + "',"
					+ "'" + slaveStatus.getChannelName() + "')";
			i++;
		}
		perfStatInsertDAO.insertSlaveStatus(values);
	}

	@Transactional
	public void insertInnodbBufferPoolStats(AppSnap appSnap,
			List<InnodbBufferPoolStats> list) {
		String values = "";
		int i = 0;
		if(list == null || list.size() == 0) {
			return;
		}
		for(InnodbBufferPoolStats stats : list) {
			if(i>0) {
				values = values + ",";
			}
			values = values + "(" 
					+ "'" + appSnap.getHostname()  + "',"
					+ "'" + appSnap.getAppname()  + "',"
					+ "'" + appSnap.getLogTime()  + "',"
					+ appSnap.getSnapId() + ","
					+ stats.getFreeBuffers() + ","
					+ stats.getDatabasePages() + ","
					+ stats.getOldDatabasePages() + ")";
			i++;
		}
		perfStatInsertDAO.insertInnodbBufferPoolStats(values);
	}

	@Transactional
	public void insertOsInfo(OsInfo osInfo) {
		perfStatInsertDAO.insertOsInfo(osInfo);
	}

	@Transactional
	public void insertSlowQuerySummary(AppSnap appSnap,
			List<SlowQueryLogDigest> list) {
		String values = "";
		int i = 0;
		if(list == null || list.size() == 0) {
			return;
		}
		for(SlowQueryLogDigest stats : list) {
			if(i>0) {
				values = values + ",";
			}
			values = values + "(" 
					+ "'" + appSnap.getHostname() + "',"
					+ "'" + appSnap.getAppname() + "',"
					+ "'" + appSnap.getLogTime() + "',"
					+ appSnap.getSnapId() + ","
					+ "'" + stats.getQueryId() + "',"
					+ stats.getTotalResponseTime() + ","
					+ stats.getCalls() + ","
					+ "'" + stats.getAbbrSql().replaceAll("'", "''") + "',"
					+ "'" + stats.getFullSql().replaceAll("'", "''").replaceAll("\\\\G", "") + "',"
					+ stats.getTotalRowsExamined() + ","
					+ stats.getTotalRowsEffected() + ")";
			i++;
		}
		//System.out.println("insertSlowQuerySummary values: " + values);
		perfStatInsertDAO.insertSlowQuerySummary(values);
	}
	
	public static void main(String[] args) {
		System.out.println("abc\\G" + "abc\\G".replaceAll("\\\\G", ""));
	}
	
}
