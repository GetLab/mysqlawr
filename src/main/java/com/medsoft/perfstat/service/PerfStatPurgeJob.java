package com.medsoft.perfstat.service;

/**   
 * @Title: MetadataUpdateJob.java 
 * @Package  
 * @Description: TODO(��һ�仰�������ļ���ʲô) 
 * @author zjhua@hundsun.com   
 * @date 2016��3��11�� ����11:48:28 
 * @version V1.0   
 */

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.cyl.kernel.DBContextHolder;

/**
 * @author zjhua
 *
 */
public class PerfStatPurgeJob extends QuartzJobBean {
	static final Logger logger = LoggerFactory.getLogger(PerfStatPurgeJob.class);
	
	private static SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss.SSS");

	protected void executeInternal(JobExecutionContext ctx)
			throws JobExecutionException {
		ApplicationContext applicationContext = null;
		try {
			applicationContext = getApplicationContext(ctx);
			PerfStatPurgeService service = applicationContext.getBean(PerfStatPurgeService.class);
			DBContextHolder.setTargetDataSource(MetadataContainer.DEFAULT_DS);
			service.purgeOldLog();
			logger.info(this.getClass().getName() + "执行成功，时间："
					+ sdf.format(new Date()));
		} catch (Exception e) {
			logger.error(this.getClass().getName() + "执行失败，时间："
					+ sdf.format(new Date()),e);
		}
		
	}

	private static final String APPLICATION_CONTEXT_KEY = "applicationContextKey";

	private ApplicationContext getApplicationContext(JobExecutionContext context)
			throws Exception {
		ApplicationContext appCtx = null;
		appCtx = (ApplicationContext) context.getScheduler().getContext()
				.get(APPLICATION_CONTEXT_KEY);
		if (appCtx == null) {
			throw new JobExecutionException(
					"No application context available in scheduler context for key \""
							+ APPLICATION_CONTEXT_KEY + "\"");
		}
		return appCtx;
	}
}
