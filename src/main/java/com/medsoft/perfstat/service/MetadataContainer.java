/**   
* @Title: MetadataContainer.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:15:18 
* @version V1.0   
*/ 
package com.medsoft.perfstat.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.medsoft.perfstat.pojo.AppSnap;
import com.medsoft.perfstat.pojo.Apps;

/**
 * @author zjhua
 *
 */
public class MetadataContainer {
	public static final Map<String,List<String>> metas = new ConcurrentHashMap<String,List<String>>();
	public static List<Apps> apps = new ArrayList<Apps>();
	public static List<AppSnap> appSnaps = new ArrayList<AppSnap>();
	public static final String SNAP_ID = "snapIds";
	public static final String APP_NAME = "appnames";
	public static final String HOST_NAME = "hostnames";
	public static final String APPS = "apps";
	public static final String APP_SNAPS = "appSnaps";
	public static final String DEFAULT_DS = "perfstat_ds";
	
	public static List<String> getSnapId() {
		return metas.get(SNAP_ID);
	}
	
	public static List<String> getAppname() {
		return metas.get(APP_NAME);
	}
	
	public static List<String> getHostname() {
		return metas.get(HOST_NAME);
	}

	/**
	 * @return
	 */
	public static List<AppSnap> getAppSnaps() {
		return appSnaps;
	}
}
