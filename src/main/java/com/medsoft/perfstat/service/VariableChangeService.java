/**   
* @Title: VariableChangeService.java 
* @Package com.medsoft.perfstat.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午5:39:29 
* @version V1.0   
*/ 
package com.medsoft.perfstat.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medsoft.perfstat.dao.GlobalVariablesChangeDAO;
import com.medsoft.perfstat.pojo.ISGlobalVariableChange;

/**
 * @author zjhua
 *
 */
@Service
public class VariableChangeService {
	@Autowired
	private GlobalVariablesChangeDAO dao;
	
	//@Transactional
	public List<ISGlobalVariableChange> queryGlobalVariablesChange(Map<String,String> param) {
		return dao.queryGlobalVariablesChange(param);
	}
}
