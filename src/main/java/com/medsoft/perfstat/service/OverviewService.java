/**   
* @Title: OverviewService.java 
* @Package com.medsoft.perfstat.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午5:38:47 
* @version V1.0   
*/ 
package com.medsoft.perfstat.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medsoft.perfstat.dao.MetadataDAO;
import com.medsoft.perfstat.dao.OverviewDAO;
import com.medsoft.perfstat.pojo.AppSnap;
import com.medsoft.perfstat.pojo.AppSnapReq;
import com.medsoft.perfstat.pojo.ISGlobalStatus;
import com.medsoft.perfstat.pojo.ISGlobalStatusChange;
import com.medsoft.perfstat.pojo.ISGlobalVariable;
import com.medsoft.perfstat.pojo.ISIndexStatistics;
import com.medsoft.perfstat.pojo.ISTableStatistics;
import com.medsoft.perfstat.pojo.OsInfo;
import com.medsoft.perfstat.pojo.PSEventsWaitsSummaryGlobalByEventName;
import com.medsoft.perfstat.pojo.SlaveStatus;
import com.medsoft.perfstat.pojo.Top10ObjectIOWait;
import com.medsoft.perfstat.pojo.Top10SQL;
import com.medsoft.perfstat.pojo.TopSlowQueryLogDigest;

/**
 * @author zjhua
 *
 */
@Service
public class OverviewService {
	@Autowired
	private MetadataDAO metadataDAO;
	
	@Autowired
	private OverviewDAO overviewDAO;
	
	@Transactional
	public List<ISGlobalVariable> queryHead(Map<String, String> params) {
		return overviewDAO.queryHead(params);
	};
	@Transactional
	public List<ISGlobalVariable> queryCritialVariales(Map<String, String> params) {
		return overviewDAO.queryCritialVariales(params);
	};
	@Transactional
	public List<ISGlobalVariable> querySlaveVariables(Map<String, String> params) {
		return overviewDAO.querySlaveVariables(params);
	};
	@Transactional
	public List<ISGlobalVariable> queryGaleraClusterVariales(Map<String, String> params) {
		return overviewDAO.queryGaleraClusterVariales(params);
	};
	@Transactional
	public List<ISGlobalVariable> queryPerfSchema(Map<String, String> params) {
		return overviewDAO.queryPerfSchema(params);
	};
	@Transactional
	public List<ISGlobalStatus> queryEfficiency(Map<String, String> params) {
		return overviewDAO.queryEfficiency(params);
	};
	@Transactional
	public List<ISGlobalStatusChange> queryCriticalStatus(Map<String, String> params) {
		return overviewDAO.queryCriticalStatus(params);
	};
	@Transactional
	public List<ISGlobalStatus> queryGaleraClusterInfo(Map<String, String> params) {
		return overviewDAO.queryGaleraClusterInfo(params);
	};
	@Transactional
	public List<Top10SQL> queryTop10ElapsedSQL(Map<String, String> params) {
		return overviewDAO.queryTop10ElapsedSQL(params);
	};
	@Transactional
	public List<Top10SQL> queryTop10RowsExamSQL(Map<String, String> params) {
		return overviewDAO.queryTop10RowsExamSQL(params);
	};
	@Transactional
	public List<Top10ObjectIOWait> queryTop10TableIOWait(Map<String, String> params) {
		return overviewDAO.queryTop10TableIOWait(params);
	};
	@Transactional
	public List<Top10ObjectIOWait> queryTop10IndexIOWait(Map<String, String> params) {
		return overviewDAO.queryTop10IndexIOWait(params);
	};
	@Transactional
	public List<ISIndexStatistics> queryTop10IndexRowsRead(Map<String, String> params) {
		return overviewDAO.queryTop10IndexRowsRead(params);
	};
	@Transactional
	public List<ISTableStatistics> queryTop10TableRowsRead(Map<String, String> params) {
		return overviewDAO.queryTop10TableRowsRead(params);
	}
	@Transactional
	public List<AppSnap> querySnapIdByAppname(String appname) {
		return metadataDAO.queryAppSnaps(appname);
	}
	@Transactional
	public List<SlaveStatus> querySlaveInfo(Map<String, String> params) {
		return overviewDAO.querySlaveStatus(params);
	}
	
	@Transactional
	public List<OsInfo> queryOsInfo(Map<String, String> map) {
		return overviewDAO.queryOsInfo(map);
	}
	public boolean isMySQLRestarted(AppSnapReq req) {
		ISGlobalStatus beginStartTime = overviewDAO.getGlobalStatus(req.getAppname(),req.getBeginSnapId(),"mysql_start_time"); 
		ISGlobalStatus endStartTime = overviewDAO.getGlobalStatus(req.getAppname(),req.getEndSnapId(),"mysql_start_time"); 
		if(beginStartTime != null && endStartTime != null && beginStartTime.getVariableValue().equals(endStartTime.getVariableValue())) {
			return false;
		}
		return true;
	}
	public List<PSEventsWaitsSummaryGlobalByEventName> queryTop10EventDetail(Map<String, String> params) {
		return overviewDAO.queryTop10EventDetail(params);
	};
	public List<PSEventsWaitsSummaryGlobalByEventName> queryTop10EventSummary(Map<String, String> params) {
		return overviewDAO.queryTop10EventSummary(params);
	}
	public List<TopSlowQueryLogDigest> queryTopSlowQueryLogDigest(
			Map<String, String> map) {
		return overviewDAO.queryTop10SlowQueryLogDigest(map);
	};
}
