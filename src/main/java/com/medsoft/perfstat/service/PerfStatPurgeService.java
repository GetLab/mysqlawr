/**   
* @Title: MetadataUpdateService.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:15:32 
* @version V1.0   
*/ 
package com.medsoft.perfstat.service;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medsoft.perfstat.dao.PerfStatPurgeDAO;
import com.medsoft.perfstat.pojo.AppSnap;

/**
 * @author zjhua
 *
 */
@Service
public class PerfStatPurgeService implements InitializingBean {
	@Autowired
	private PerfStatPurgeDAO perfStatPurgeDAO;
	
	@Transactional
	public void purgeOldLog() {
		String lastWeekDate = perfStatPurgeDAO.getLastWeekDate();
		perfStatPurgeDAO.deleteAppSnaps(lastWeekDate);
		perfStatPurgeDAO.deleteISGlobalStatus(lastWeekDate);
		perfStatPurgeDAO.deleteISGlobalVariables(lastWeekDate);
		perfStatPurgeDAO.deleteISIndexStatistics(lastWeekDate);
		perfStatPurgeDAO.deleteISTableStatistics(lastWeekDate);
		perfStatPurgeDAO.deletePSEventsStatementsSummaryByDigest(lastWeekDate);
		perfStatPurgeDAO.deletePSEventsWaitsSummaryGlobalByEventName(lastWeekDate);
		perfStatPurgeDAO.deletePSTableIOWaitsSummaryByIndexUsage(lastWeekDate);
		perfStatPurgeDAO.deletePSTableIOWaitsSummaryByTable(lastWeekDate);
		perfStatPurgeDAO.deletePSTableLockWaitsSummaryByTable(lastWeekDate);
		perfStatPurgeDAO.deleteSlowQuerySummary(lastWeekDate);
	}
	
	@Transactional
	public void deleteOldLog(AppSnap appSnap) {
		perfStatPurgeDAO.deleteAppSnaps(appSnap);
		perfStatPurgeDAO.deleteISGlobalStatus(appSnap);
		perfStatPurgeDAO.deleteISGlobalVariables(appSnap);
		perfStatPurgeDAO.deleteISIndexStatistics(appSnap);
		perfStatPurgeDAO.deleteISTableStatistics(appSnap);
		perfStatPurgeDAO.deletePSEventsStatementsSummaryByDigest(appSnap);
		perfStatPurgeDAO.deletePSEventsWaitsSummaryGlobalByEventName(appSnap);
		perfStatPurgeDAO.deletePSTableIOWaitsSummaryByIndexUsage(appSnap);
		perfStatPurgeDAO.deletePSTableIOWaitsSummaryByTable(appSnap);
		perfStatPurgeDAO.deletePSTableLockWaitsSummaryByTable(appSnap);
		perfStatPurgeDAO.deleteSlowQuerySummary(appSnap);
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	//@Transactional
	@Override
	public void afterPropertiesSet() throws Exception {
		/*perfStatPurgeDAO.deleteAppSnaps();
		perfStatPurgeDAO.deleteISGlobalStatus();
		perfStatPurgeDAO.deleteISGlobalVariables();
		perfStatPurgeDAO.deleteISIndexStatistics();
		perfStatPurgeDAO.deleteISTableStatistics();
		perfStatPurgeDAO.deletePSEventsStatementsSummaryByDigest();
		perfStatPurgeDAO.deletePSEventsWaitsSummaryGlobalByEventName();
		perfStatPurgeDAO.deletePSTableIOWaitsSummaryByIndexUsage();
		perfStatPurgeDAO.deletePSTableIOWaitsSummaryByTable();
		perfStatPurgeDAO.deletePSTableLockWaitsSummaryByTable();
		*/
	}
}
