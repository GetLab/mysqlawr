/**   
* @Title: MetadataUpdateService.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月12日 上午9:15:32 
* @version V1.0   
*/ 
package com.medsoft.perfstat.service;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medsoft.perfstat.dao.MetadataDAO;

/**
 * @author zjhua
 *
 */
@Service
public class MetadataUpdateService implements InitializingBean {
	@Autowired
	private MetadataDAO metadataDAO;
	
	//@Transactional
	public void updateMetadata() {
		MetadataContainer.metas.put(MetadataContainer.SNAP_ID, metadataDAO.querySnapId());
		MetadataContainer.metas.put(MetadataContainer.APP_NAME, metadataDAO.queryAppname());
		MetadataContainer.metas.put(MetadataContainer.HOST_NAME, metadataDAO.queryHostname());
		MetadataContainer.apps = metadataDAO.queryApps("",0);
		MetadataContainer.appSnaps = metadataDAO.queryAppSnaps("");
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	//@Transactional
	@Override
	public void afterPropertiesSet() throws Exception {
		MetadataContainer.metas.put(MetadataContainer.SNAP_ID, metadataDAO.querySnapId());
		MetadataContainer.metas.put(MetadataContainer.APP_NAME, metadataDAO.queryAppname());
		MetadataContainer.metas.put(MetadataContainer.HOST_NAME, metadataDAO.queryHostname());
		MetadataContainer.apps = metadataDAO.queryApps("",0);
		MetadataContainer.appSnaps = metadataDAO.queryAppSnaps("");
	}
}
