/**   
* @Title: StatusChangeService.java 
* @Package com.medsoft.perfstat.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午5:39:14 
* @version V1.0   
*/ 
package com.medsoft.perfstat.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.medsoft.perfstat.dao.GlobalStatusChangeDAO;
import com.medsoft.perfstat.pojo.ISGlobalStatusChange;

/**
 * @author zjhua
 *
 */
@Service
public class StatusChangeService {
	@Autowired
	private GlobalStatusChangeDAO dao;
	
	//@Transactional
	public List<ISGlobalStatusChange> queryGlobalStatusChange(Map<String,String> param) {
		return dao.queryGlobalStatusChange(param);
	}
}
