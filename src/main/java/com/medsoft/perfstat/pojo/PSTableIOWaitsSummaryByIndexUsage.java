/**   
* @Title: PSTableIOWaitsSummaryByIndexUsage.java 
* @Package com.medsoft.perfstat.pojo 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午9:39:17 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

/**
 * @author zjhua
 *
 */
public class PSTableIOWaitsSummaryByIndexUsage extends AppSnap {
	private String objectType;
	private String objectSchema;
	private String objectName;
	private String indexName;
	private long countStar;
	private long sumTimerWait;
	private long minTimerWait;
	private long maxTimerWait;
	private long avgTimerWait;
	private long countRead;
	private long sumTimerRead;
	private long minTimerRead;
	private long maxTimerRead;
	private long avgTimerRead;
	private long countWrite;
	private long sumTimerWrite;
	private long minTimerWrite;
	private long maxTimerWrite;
	private long avgTimerWrite;
	private long countFetch;
	private long sumTimerFetch;
	private long minTimerFetch;
	private long maxTimerFetch;
	private long avgTimerFetch;
	
	private long countInsert;
	private long sumTimerInsert;
	private long minTimerInsert;
	private long maxTimerInsert;
	private long avgTimerInsert;
	
	private long countUpdate;
	private long sumTimerUpdate;
	private long minTimerUpdate;
	private long maxTimerUpdate;
	private long avgTimerUpdate;
	
	private long countDelete;
	private long sumTimerDelete;
	private long minTimerDelete;
	private long maxTimerDelete;
	private long avgTimerDelete;
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public String getObjectSchema() {
		return objectSchema;
	}
	public void setObjectSchema(String objectSchema) {
		this.objectSchema = objectSchema;
	}
	public String getObjectName() {
		return objectName;
	}
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	public String getIndexName() {
		return indexName;
	}
	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}
	public long getCountStar() {
		return countStar;
	}
	public void setCountStar(long countStar) {
		this.countStar = countStar;
	}
	public long getSumTimerWait() {
		return sumTimerWait;
	}
	public void setSumTimerWait(long sumTimerWait) {
		this.sumTimerWait = sumTimerWait;
	}
	public long getMinTimerWait() {
		return minTimerWait;
	}
	public void setMinTimerWait(long minTimerWait) {
		this.minTimerWait = minTimerWait;
	}
	public long getMaxTimerWait() {
		return maxTimerWait;
	}
	public void setMaxTimerWait(long maxTimerWait) {
		this.maxTimerWait = maxTimerWait;
	}
	public long getAvgTimerWait() {
		return avgTimerWait;
	}
	public void setAvgTimerWait(long avgTimerWait) {
		this.avgTimerWait = avgTimerWait;
	}
	public long getCountRead() {
		return countRead;
	}
	public void setCountRead(long countRead) {
		this.countRead = countRead;
	}
	public long getSumTimerRead() {
		return sumTimerRead;
	}
	public void setSumTimerRead(long sumTimerRead) {
		this.sumTimerRead = sumTimerRead;
	}
	public long getMinTimerRead() {
		return minTimerRead;
	}
	public void setMinTimerRead(long minTimerRead) {
		this.minTimerRead = minTimerRead;
	}
	public long getMaxTimerRead() {
		return maxTimerRead;
	}
	public void setMaxTimerRead(long maxTimerRead) {
		this.maxTimerRead = maxTimerRead;
	}
	public long getAvgTimerRead() {
		return avgTimerRead;
	}
	public void setAvgTimerRead(long avgTimerRead) {
		this.avgTimerRead = avgTimerRead;
	}
	public long getCountWrite() {
		return countWrite;
	}
	public void setCountWrite(long countWrite) {
		this.countWrite = countWrite;
	}
	public long getSumTimerWrite() {
		return sumTimerWrite;
	}
	public void setSumTimerWrite(long sumTimerWrite) {
		this.sumTimerWrite = sumTimerWrite;
	}
	public long getMinTimerWrite() {
		return minTimerWrite;
	}
	public void setMinTimerWrite(long minTimerWrite) {
		this.minTimerWrite = minTimerWrite;
	}
	public long getMaxTimerWrite() {
		return maxTimerWrite;
	}
	public void setMaxTimerWrite(long maxTimerWrite) {
		this.maxTimerWrite = maxTimerWrite;
	}
	public long getAvgTimerWrite() {
		return avgTimerWrite;
	}
	public void setAvgTimerWrite(long avgTimerWrite) {
		this.avgTimerWrite = avgTimerWrite;
	}
	public long getCountFetch() {
		return countFetch;
	}
	public void setCountFetch(long countFetch) {
		this.countFetch = countFetch;
	}
	public long getSumTimerFetch() {
		return sumTimerFetch;
	}
	public void setSumTimerFetch(long sumTimerFetch) {
		this.sumTimerFetch = sumTimerFetch;
	}
	public long getMinTimerFetch() {
		return minTimerFetch;
	}
	public void setMinTimerFetch(long minTimerFetch) {
		this.minTimerFetch = minTimerFetch;
	}
	public long getMaxTimerFetch() {
		return maxTimerFetch;
	}
	public void setMaxTimerFetch(long maxTimerFetch) {
		this.maxTimerFetch = maxTimerFetch;
	}
	public long getAvgTimerFetch() {
		return avgTimerFetch;
	}
	public void setAvgTimerFetch(long avgTimerFetch) {
		this.avgTimerFetch = avgTimerFetch;
	}
	public long getCountInsert() {
		return countInsert;
	}
	public void setCountInsert(long countInsert) {
		this.countInsert = countInsert;
	}
	public long getSumTimerInsert() {
		return sumTimerInsert;
	}
	public void setSumTimerInsert(long sumTimerInsert) {
		this.sumTimerInsert = sumTimerInsert;
	}
	public long getMinTimerInsert() {
		return minTimerInsert;
	}
	public void setMinTimerInsert(long minTimerInsert) {
		this.minTimerInsert = minTimerInsert;
	}
	public long getMaxTimerInsert() {
		return maxTimerInsert;
	}
	public void setMaxTimerInsert(long maxTimerInsert) {
		this.maxTimerInsert = maxTimerInsert;
	}
	public long getAvgTimerInsert() {
		return avgTimerInsert;
	}
	public void setAvgTimerInsert(long avgTimerInsert) {
		this.avgTimerInsert = avgTimerInsert;
	}
	public long getCountUpdate() {
		return countUpdate;
	}
	public void setCountUpdate(long countUpdate) {
		this.countUpdate = countUpdate;
	}
	public long getSumTimerUpdate() {
		return sumTimerUpdate;
	}
	public void setSumTimerUpdate(long sumTimerUpdate) {
		this.sumTimerUpdate = sumTimerUpdate;
	}
	public long getMinTimerUpdate() {
		return minTimerUpdate;
	}
	public void setMinTimerUpdate(long minTimerUpdate) {
		this.minTimerUpdate = minTimerUpdate;
	}
	public long getMaxTimerUpdate() {
		return maxTimerUpdate;
	}
	public void setMaxTimerUpdate(long maxTimerUpdate) {
		this.maxTimerUpdate = maxTimerUpdate;
	}
	public long getAvgTimerUpdate() {
		return avgTimerUpdate;
	}
	public void setAvgTimerUpdate(long avgTimerUpdate) {
		this.avgTimerUpdate = avgTimerUpdate;
	}
	public long getCountDelete() {
		return countDelete;
	}
	public void setCountDelete(long countDelete) {
		this.countDelete = countDelete;
	}
	public long getSumTimerDelete() {
		return sumTimerDelete;
	}
	public void setSumTimerDelete(long sumTimerDelete) {
		this.sumTimerDelete = sumTimerDelete;
	}
	public long getMinTimerDelete() {
		return minTimerDelete;
	}
	public void setMinTimerDelete(long minTimerDelete) {
		this.minTimerDelete = minTimerDelete;
	}
	public long getMaxTimerDelete() {
		return maxTimerDelete;
	}
	public void setMaxTimerDelete(long maxTimerDelete) {
		this.maxTimerDelete = maxTimerDelete;
	}
	public long getAvgTimerDelete() {
		return avgTimerDelete;
	}
	public void setAvgTimerDelete(long avgTimerDelete) {
		this.avgTimerDelete = avgTimerDelete;
	}
}
