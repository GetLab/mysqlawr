/**   
* @Title: ISIndexStatistics.java 
* @Package com.medsoft.perfstat.pojo 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午9:41:44 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

/**
 * @author zjhua
 *
 */
public class ISIndexStatistics extends AppSnap {
	private String tableSchema;
	private String tableName;
	private String indexName;
	private long rowsRead;
	public String getTableSchema() {
		return tableSchema;
	}
	public void setTableSchema(String tableSchema) {
		this.tableSchema = tableSchema;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getIndexName() {
		return indexName;
	}
	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}
	public long getRowsRead() {
		return rowsRead;
	}
	public void setRowsRead(long rowsRead) {
		this.rowsRead = rowsRead;
	}
}
