/**   
* @Title: ISGlobalStatus.java 
* @Package com.medsoft.perfstat.core 
* @Description: (用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午9:31:59 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;


/**
 * @author zjhua
 *
 */
public class ISGlobalStatusChange extends AppSnap {
	private String variableName;
	private String preVariableValue;
	private String curVariableValue;
	private String differenceValue;
	
	public String getVariableName() {
		return variableName;
	}
	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}
	public String getPreVariableValue() {
		return preVariableValue;
	}
	public void setPreVariableValue(String preVariableValue) {
		this.preVariableValue = preVariableValue;
	}
	public String getCurVariableValue() {
		return curVariableValue;
	}
	public void setCurVariableValue(String curVariableValue) {
		this.curVariableValue = curVariableValue;
	}
	public String getDifferenceValue() {
		try {
			return String.valueOf(Math.round((Double.parseDouble(this.curVariableValue) - Double.parseDouble(this.preVariableValue))));
		} catch(NumberFormatException e) {
			return "不适用";
		}
	}
	public void setDifferenceValue(String differenceValue) {
		this.differenceValue = differenceValue;
	}
	
	public static void main(String[] args) {
		try {
			System.out.println(Double.parseDouble("a12") - Double.parseDouble("2"));
		} catch(NumberFormatException e) {
			System.out.println("不适用");
		}
	}
}
