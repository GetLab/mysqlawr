package com.medsoft.perfstat.pojo;

public class SlaveStatus {
	private String slaveIoState;
	private String masterHost;
	private String masterUser;
	private String masterPort;
	private String connectRetry;
	private String masterLogFile;
	private String readMasterLogPos;
	private String relayLogFile;
	private String relayLogPos;
	private String relayMasterLogFile;
	private String slaveIoRunning;
	private String slaveSqlRunning;
	private String replicateDoDb;
	private String replicateIgnoreDb;
	private String replicateDoTable;
	private String replicateIgnoreTable;
	private String replicateWildDoTable;
	private String replicateWildIgnoreTable;
	private String lastErrno;
	private String lastError;
	private String skipCounter;
	private String execMasterLogPos;
	private String relayLogSpace;
	private String untilCondition;
	private String untilLogFile;
	private String untilLogPos;
	private String secondsBehindMaster;
	private String lastIoErrno;
	private String lastIoError;
	private String lastSqlErrno;
	private String lastSqlError;
	private String replicateIgnoreServerIds;
	private String masterServerId;
	private String masterUuid;
	private String masterInfoFile;
	private String sqlDelay;
	private String sqlRemainingDelay;
	private String slaveSqlRunningState;
	private String masterRetryCount;
	private String masterBind;
	private String lastIoErrorTimestamp;
	private String lastSqlErrorTimestamp;
	private String retrievedGtidSet;
	private String executedGtidSet;
	private String autoPosition;
	private String replicateRewriteDb;
	private String channelName;
	public String getSlaveIoState() {
		return slaveIoState;
	}
	public void setSlaveIoState(String slaveIoState) {
		this.slaveIoState = slaveIoState;
	}
	public String getMasterHost() {
		return masterHost;
	}
	public void setMasterHost(String masterHost) {
		this.masterHost = masterHost;
	}
	public String getMasterUser() {
		return masterUser;
	}
	public void setMasterUser(String masterUser) {
		this.masterUser = masterUser;
	}
	public String getMasterPort() {
		return masterPort;
	}
	public void setMasterPort(String masterPort) {
		this.masterPort = masterPort;
	}
	public String getConnectRetry() {
		return connectRetry;
	}
	public void setConnectRetry(String connectRetry) {
		this.connectRetry = connectRetry;
	}
	public String getMasterLogFile() {
		return masterLogFile;
	}
	public void setMasterLogFile(String masterLogFile) {
		this.masterLogFile = masterLogFile;
	}
	public String getReadMasterLogPos() {
		return readMasterLogPos;
	}
	public void setReadMasterLogPos(String readMasterLogPos) {
		this.readMasterLogPos = readMasterLogPos;
	}
	public String getRelayLogFile() {
		return relayLogFile;
	}
	public void setRelayLogFile(String relayLogFile) {
		this.relayLogFile = relayLogFile;
	}
	public String getRelayLogPos() {
		return relayLogPos;
	}
	public void setRelayLogPos(String relayLogPos) {
		this.relayLogPos = relayLogPos;
	}
	public String getRelayMasterLogFile() {
		return relayMasterLogFile;
	}
	public void setRelayMasterLogFile(String relayMasterLogFile) {
		this.relayMasterLogFile = relayMasterLogFile;
	}
	public String getSlaveIoRunning() {
		return slaveIoRunning;
	}
	public void setSlaveIoRunning(String slaveIoRunning) {
		this.slaveIoRunning = slaveIoRunning;
	}
	public String getSlaveSqlRunning() {
		return slaveSqlRunning;
	}
	public void setSlaveSqlRunning(String slaveSqlRunning) {
		this.slaveSqlRunning = slaveSqlRunning;
	}
	public String getReplicateDoDb() {
		return replicateDoDb;
	}
	public void setReplicateDoDb(String replicateDoDb) {
		this.replicateDoDb = replicateDoDb;
	}
	public String getReplicateIgnoreDb() {
		return replicateIgnoreDb;
	}
	public void setReplicateIgnoreDb(String replicateIgnoreDb) {
		this.replicateIgnoreDb = replicateIgnoreDb;
	}
	public String getReplicateDoTable() {
		return replicateDoTable;
	}
	public void setReplicateDoTable(String replicateDoTable) {
		this.replicateDoTable = replicateDoTable;
	}
	public String getReplicateIgnoreTable() {
		return replicateIgnoreTable;
	}
	public void setReplicateIgnoreTable(String replicateIgnoreTable) {
		this.replicateIgnoreTable = replicateIgnoreTable;
	}
	public String getReplicateWildDoTable() {
		return replicateWildDoTable;
	}
	public void setReplicateWildDoTable(String replicateWildDoTable) {
		this.replicateWildDoTable = replicateWildDoTable;
	}
	public String getReplicateWildIgnoreTable() {
		return replicateWildIgnoreTable;
	}
	public void setReplicateWildIgnoreTable(String replicateWildIgnoreTable) {
		this.replicateWildIgnoreTable = replicateWildIgnoreTable;
	}
	public String getLastErrno() {
		return lastErrno;
	}
	public void setLastErrno(String lastErrno) {
		this.lastErrno = lastErrno;
	}
	public String getLastError() {
		return lastError;
	}
	public void setLastError(String lastError) {
		this.lastError = lastError;
	}
	public String getSkipCounter() {
		return skipCounter;
	}
	public void setSkipCounter(String skipCounter) {
		this.skipCounter = skipCounter;
	}
	public String getExecMasterLogPos() {
		return execMasterLogPos;
	}
	public void setExecMasterLogPos(String execMasterLogPos) {
		this.execMasterLogPos = execMasterLogPos;
	}
	public String getRelayLogSpace() {
		return relayLogSpace;
	}
	public void setRelayLogSpace(String relayLogSpace) {
		this.relayLogSpace = relayLogSpace;
	}
	public String getUntilCondition() {
		return untilCondition;
	}
	public void setUntilCondition(String untilCondition) {
		this.untilCondition = untilCondition;
	}
	public String getUntilLogFile() {
		return untilLogFile;
	}
	public void setUntilLogFile(String untilLogFile) {
		this.untilLogFile = untilLogFile;
	}
	public String getUntilLogPos() {
		return untilLogPos;
	}
	public void setUntilLogPos(String untilLogPos) {
		this.untilLogPos = untilLogPos;
	}
	public String getSecondsBehindMaster() {
		return secondsBehindMaster;
	}
	public void setSecondsBehindMaster(String secondsBehindMaster) {
		this.secondsBehindMaster = secondsBehindMaster;
	}
	public String getLastIoErrno() {
		return lastIoErrno;
	}
	public void setLastIoErrno(String lastIoErrno) {
		this.lastIoErrno = lastIoErrno;
	}
	public String getLastIoError() {
		return lastIoError;
	}
	public void setLastIoError(String lastIoError) {
		this.lastIoError = lastIoError;
	}
	public String getLastSqlErrno() {
		return lastSqlErrno;
	}
	public void setLastSqlErrno(String lastSqlErrno) {
		this.lastSqlErrno = lastSqlErrno;
	}
	public String getLastSqlError() {
		return lastSqlError;
	}
	public void setLastSqlError(String lastSqlError) {
		this.lastSqlError = lastSqlError;
	}
	public String getReplicateIgnoreServerIds() {
		return replicateIgnoreServerIds;
	}
	public void setReplicateIgnoreServerIds(String replicateIgnoreServerIds) {
		this.replicateIgnoreServerIds = replicateIgnoreServerIds;
	}
	public String getMasterServerId() {
		return masterServerId;
	}
	public void setMasterServerId(String masterServerId) {
		this.masterServerId = masterServerId;
	}
	public String getMasterUuid() {
		return masterUuid;
	}
	public void setMasterUuid(String masterUuid) {
		this.masterUuid = masterUuid;
	}
	public String getMasterInfoFile() {
		return masterInfoFile;
	}
	public void setMasterInfoFile(String masterInfoFile) {
		this.masterInfoFile = masterInfoFile;
	}
	public String getSqlDelay() {
		return sqlDelay;
	}
	public void setSqlDelay(String sqlDelay) {
		this.sqlDelay = sqlDelay;
	}
	public String getSqlRemainingDelay() {
		return sqlRemainingDelay;
	}
	public void setSqlRemainingDelay(String sqlRemainingDelay) {
		this.sqlRemainingDelay = sqlRemainingDelay;
	}
	public String getSlaveSqlRunningState() {
		return slaveSqlRunningState;
	}
	public void setSlaveSqlRunningState(String slaveSqlRunningState) {
		this.slaveSqlRunningState = slaveSqlRunningState;
	}
	public String getMasterRetryCount() {
		return masterRetryCount;
	}
	public void setMasterRetryCount(String masterRetryCount) {
		this.masterRetryCount = masterRetryCount;
	}
	public String getMasterBind() {
		return masterBind;
	}
	public void setMasterBind(String masterBind) {
		this.masterBind = masterBind;
	}
	public String getLastIoErrorTimestamp() {
		return lastIoErrorTimestamp;
	}
	public void setLastIoErrorTimestamp(String lastIoErrorTimestamp) {
		this.lastIoErrorTimestamp = lastIoErrorTimestamp;
	}
	public String getLastSqlErrorTimestamp() {
		return lastSqlErrorTimestamp;
	}
	public void setLastSqlErrorTimestamp(String lastSqlErrorTimestamp) {
		this.lastSqlErrorTimestamp = lastSqlErrorTimestamp;
	}
	public String getRetrievedGtidSet() {
		return retrievedGtidSet;
	}
	public void setRetrievedGtidSet(String retrievedGtidSet) {
		this.retrievedGtidSet = retrievedGtidSet;
	}
	public String getExecutedGtidSet() {
		return executedGtidSet;
	}
	public void setExecutedGtidSet(String executedGtidSet) {
		this.executedGtidSet = executedGtidSet;
	}
	public String getAutoPosition() {
		return autoPosition;
	}
	public void setAutoPosition(String autoPosition) {
		this.autoPosition = autoPosition;
	}
	public String getReplicateRewriteDb() {
		return replicateRewriteDb;
	}
	public void setReplicateRewriteDb(String replicateRewriteDb) {
		this.replicateRewriteDb = replicateRewriteDb;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
}
