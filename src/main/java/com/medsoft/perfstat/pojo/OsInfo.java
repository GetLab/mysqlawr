package com.medsoft.perfstat.pojo;

public class OsInfo extends AppSnap {
	private int cores;
	private double systemTime;
	private double userTime;
	private double ioTime;
	private double idleTime;
	private double totalTime;
	private double totalBusyTime;
	private double systemTimePct;
	private double userTimePct;
	private double ioTimePct;
	private double totalBusyTimePct;
	private long upTime;
	private long memTotal;
	private long memUsed;
	private long swapTotal;
	private long swapUsed;
	
	public double getTotalTime() {
		return (systemTime + userTime + ioTime + idleTime);
	}
	
	public double getTotalBusyTime() {
		return (systemTime + userTime + ioTime);
	}
	public double getSystemTimePct() {
		return Math.round(systemTime/getTotalTime()*100);
	}
	public double getTotalBusyTimePct() {
		return Math.round(getTotalBusyTime()/getTotalTime()*100);
	}
	public double getUserTimePct() {
		return Math.round(userTime/getTotalTime()*100);
	}
	public double getIoTimePct() {
		return Math.round(ioTime/getTotalTime()*100);
	}
	public void setIoTime(double ioTime) {
		this.ioTime = ioTime;
	}
	public int getCores() {
		return cores;
	}
	public void setCores(int cores) {
		this.cores = cores;
	}
	public double getSystemTime() {
		return systemTime;
	}
	public void setSystemTime(double systemTime) {
		this.systemTime = systemTime;
	}
	public double getUserTime() {
		return userTime;
	}
	public void setUserTime(double userTime) {
		this.userTime = userTime;
	}
	public double getIoTime() {
		return ioTime;
	}
	public void setIoTime(long ioTime) {
		this.ioTime = ioTime;
	}
	public double getIdleTime() {
		return idleTime;
	}
	public void setIdleTime(double idleTime) {
		this.idleTime = idleTime;
	}
	public long getUpTime() {
		return upTime;
	}
	public void setUpTime(long upTime) {
		this.upTime = upTime;
	}
	public long getMemTotal() {
		return memTotal;
	}
	public void setMemTotal(long memTotal) {
		this.memTotal = memTotal;
	}
	public long getMemUsed() {
		return memUsed;
	}
	public void setMemUsed(long memUsed) {
		this.memUsed = memUsed;
	}
	public long getSwapTotal() {
		return swapTotal;
	}
	public void setSwapTotal(long swapTotal) {
		this.swapTotal = swapTotal;
	}
	public long getSwapUsed() {
		return swapUsed;
	}
	public void setSwapUsed(long swapUsed) {
		this.swapUsed = swapUsed;
	}
	
	public void setAppSnap(AppSnap appSnap) {
		super.setAppname(appSnap.getAppname());
		super.setHostname(appSnap.getHostname());
		super.setLogTime(appSnap.getLogTime());
		super.setSnapId(appSnap.getSnapId());
	}
}
