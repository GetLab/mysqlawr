package com.medsoft.perfstat.pojo;

public class ISTable {
	private String tableSchema;
	private String tableName;
	private long tableRows;
	private long dataLength;
	private int avgRowLength;
	private long indexLength;
	public String getTableSchema() {
		return tableSchema;
	}
	public void setTableSchema(String tableSchema) {
		this.tableSchema = tableSchema;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public long getTableRows() {
		return tableRows;
	}
	public void setTableRows(long tableRows) {
		this.tableRows = tableRows;
	}
	public long getDataLength() {
		return dataLength;
	}
	public void setDataLength(long dataLength) {
		this.dataLength = dataLength;
	}
	public int getAvgRowLength() {
		return avgRowLength;
	}
	public void setAvgRowLength(int avgRowLength) {
		this.avgRowLength = avgRowLength;
	}
	public long getIndexLength() {
		return indexLength;
	}
	public void setIndexLength(long indexLength) {
		this.indexLength = indexLength;
	}
}
