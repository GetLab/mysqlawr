/**   
* @Title: PSTableLockWaitsSummaryByTable.java 
* @Package com.medsoft.perfstat.pojo 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午9:38:49 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

/**
 * @author zjhua
 *
 */
public class PSTableLockWaitsSummaryByTable extends AppSnap {
	private String objectType;
	private String objectSchema;
	private String objectName;
	private long countStar;
	private long sumTimerWait;
	private long minTimerWait;
	private long maxTimerWait;
	private long avgTimerWait;
	private long countRead;
	private long sumTimerRead;
	private long minTimerRead;
	private long maxTimerRead;
	private long avgTimerRead;
	private long countWrite;
	private long sumTimerWrite;
	private long minTimerWrite;
	private long maxTimerWrite;
	private long avgTimerWrite;
	
	private long countReadNormal;
	private long sumTimerReadNormal;
	private long minTimerReadNormal;
	private long avgTimerReadNormal;
	private long maxTimerReadNormal;
	private long countReadWithSharedLocks;
	private long sumTimerReadWithSharedLocks;
	private long minTimerReadWithSharedLocks;
	private long avgTimerReadWithSharedLocks;
	private long maxTimerReadWithSharedLocks;
	private long countReadHighPriority;
	private long sumTimerReadHighPriority;
	private long minTimerReadHighPriority;
	private long avgTimerReadHighPriority;
	private long maxTimerReadHighPriority;
	private long countReadNoInsert;
	private long sumTimerReadNoInsert;
	private long minTimerReadNoInsert;
	private long avgTimerReadNoInsert;
	private long maxTimerReadNoInsert;
	private long countReadExternal;
	private long sumTimerReadExternal;
	private long minTimerReadExternal;
	private long avgTimerReadExternal;
	private long maxTimerReadExternal;
	private long countWriteAllowWrite;
	private long sumTimerWriteAllowWrite;
	private long minTimerWriteAllowWrite;
	private long avgTimerWriteAllowWrite;
	private long maxTimerWriteAllowWrite;
	private long countWriteConcurrentInsert;
	private long sumTimerWriteConcurrentInsert;
	private long minTimerWriteConcurrentInsert;
	private long avgTimerWriteConcurrentInsert;
	private long maxTimerWriteConcurrentInsert;
	private long countWriteDelayed;
	private long sumTimerWriteDelayed;
	private long minTimerWriteDelayed;
	private long avgTimerWriteDelayed;
	private long maxTimerWriteDelayed;
	private long countWriteLowPriority;
	private long sumTimerWriteLowPriority;
	private long minTimerWriteLowPriority;
	private long avgTimerWriteLowPriority;
	private long maxTimerWriteLowPriority;
	private long countWriteNormal;
	private long sumTimerWriteNormal;
	private long minTimerWriteNormal;
	private long avgTimerWriteNormal;
	private long maxTimerWriteNormal;
	private long countWriteExternal;
	private long sumTimerWriteExternal;
	private long minTimerWriteExternal;
	private long avgTimerWriteExternal;
	private long maxTimerWriteExternal;
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public String getObjectSchema() {
		return objectSchema;
	}
	public void setObjectSchema(String objectSchema) {
		this.objectSchema = objectSchema;
	}
	public String getObjectName() {
		return objectName;
	}
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	public long getCountStar() {
		return countStar;
	}
	public void setCountStar(long countStar) {
		this.countStar = countStar;
	}
	public long getSumTimerWait() {
		return sumTimerWait;
	}
	public void setSumTimerWait(long sumTimerWait) {
		this.sumTimerWait = sumTimerWait;
	}
	public long getMinTimerWait() {
		return minTimerWait;
	}
	public void setMinTimerWait(long minTimerWait) {
		this.minTimerWait = minTimerWait;
	}
	public long getMaxTimerWait() {
		return maxTimerWait;
	}
	public void setMaxTimerWait(long maxTimerWait) {
		this.maxTimerWait = maxTimerWait;
	}
	public long getAvgTimerWait() {
		return avgTimerWait;
	}
	public void setAvgTimerWait(long avgTimerWait) {
		this.avgTimerWait = avgTimerWait;
	}
	public long getCountRead() {
		return countRead;
	}
	public void setCountRead(long countRead) {
		this.countRead = countRead;
	}
	public long getSumTimerRead() {
		return sumTimerRead;
	}
	public void setSumTimerRead(long sumTimerRead) {
		this.sumTimerRead = sumTimerRead;
	}
	public long getMinTimerRead() {
		return minTimerRead;
	}
	public void setMinTimerRead(long minTimerRead) {
		this.minTimerRead = minTimerRead;
	}
	public long getMaxTimerRead() {
		return maxTimerRead;
	}
	public void setMaxTimerRead(long maxTimerRead) {
		this.maxTimerRead = maxTimerRead;
	}
	public long getAvgTimerRead() {
		return avgTimerRead;
	}
	public void setAvgTimerRead(long avgTimerRead) {
		this.avgTimerRead = avgTimerRead;
	}
	public long getCountWrite() {
		return countWrite;
	}
	public void setCountWrite(long countWrite) {
		this.countWrite = countWrite;
	}
	public long getSumTimerWrite() {
		return sumTimerWrite;
	}
	public void setSumTimerWrite(long sumTimerWrite) {
		this.sumTimerWrite = sumTimerWrite;
	}
	public long getMinTimerWrite() {
		return minTimerWrite;
	}
	public void setMinTimerWrite(long minTimerWrite) {
		this.minTimerWrite = minTimerWrite;
	}
	public long getMaxTimerWrite() {
		return maxTimerWrite;
	}
	public void setMaxTimerWrite(long maxTimerWrite) {
		this.maxTimerWrite = maxTimerWrite;
	}
	public long getAvgTimerWrite() {
		return avgTimerWrite;
	}
	public void setAvgTimerWrite(long avgTimerWrite) {
		this.avgTimerWrite = avgTimerWrite;
	}
	public long getCountReadNormal() {
		return countReadNormal;
	}
	public void setCountReadNormal(long countReadNormal) {
		this.countReadNormal = countReadNormal;
	}
	public long getSumTimerReadNormal() {
		return sumTimerReadNormal;
	}
	public void setSumTimerReadNormal(long sumTimerReadNormal) {
		this.sumTimerReadNormal = sumTimerReadNormal;
	}
	public long getMinTimerReadNormal() {
		return minTimerReadNormal;
	}
	public void setMinTimerReadNormal(long minTimerReadNormal) {
		this.minTimerReadNormal = minTimerReadNormal;
	}
	public long getAvgTimerReadNormal() {
		return avgTimerReadNormal;
	}
	public void setAvgTimerReadNormal(long avgTimerReadNormal) {
		this.avgTimerReadNormal = avgTimerReadNormal;
	}
	public long getMaxTimerReadNormal() {
		return maxTimerReadNormal;
	}
	public void setMaxTimerReadNormal(long maxTimerReadNormal) {
		this.maxTimerReadNormal = maxTimerReadNormal;
	}
	public long getCountReadWithSharedLocks() {
		return countReadWithSharedLocks;
	}
	public void setCountReadWithSharedLocks(long countReadWithSharedLocks) {
		this.countReadWithSharedLocks = countReadWithSharedLocks;
	}
	public long getSumTimerReadWithSharedLocks() {
		return sumTimerReadWithSharedLocks;
	}
	public void setSumTimerReadWithSharedLocks(long sumTimerReadWithSharedLocks) {
		this.sumTimerReadWithSharedLocks = sumTimerReadWithSharedLocks;
	}
	public long getMinTimerReadWithSharedLocks() {
		return minTimerReadWithSharedLocks;
	}
	public void setMinTimerReadWithSharedLocks(long minTimerReadWithSharedLocks) {
		this.minTimerReadWithSharedLocks = minTimerReadWithSharedLocks;
	}
	public long getAvgTimerReadWithSharedLocks() {
		return avgTimerReadWithSharedLocks;
	}
	public void setAvgTimerReadWithSharedLocks(long avgTimerReadWithSharedLocks) {
		this.avgTimerReadWithSharedLocks = avgTimerReadWithSharedLocks;
	}
	public long getMaxTimerReadWithSharedLocks() {
		return maxTimerReadWithSharedLocks;
	}
	public void setMaxTimerReadWithSharedLocks(long maxTimerReadWithSharedLocks) {
		this.maxTimerReadWithSharedLocks = maxTimerReadWithSharedLocks;
	}
	public long getCountReadHighPriority() {
		return countReadHighPriority;
	}
	public void setCountReadHighPriority(long countReadHighPriority) {
		this.countReadHighPriority = countReadHighPriority;
	}
	public long getSumTimerReadHighPriority() {
		return sumTimerReadHighPriority;
	}
	public void setSumTimerReadHighPriority(long sumTimerReadHighPriority) {
		this.sumTimerReadHighPriority = sumTimerReadHighPriority;
	}
	public long getMinTimerReadHighPriority() {
		return minTimerReadHighPriority;
	}
	public void setMinTimerReadHighPriority(long minTimerReadHighPriority) {
		this.minTimerReadHighPriority = minTimerReadHighPriority;
	}
	public long getAvgTimerReadHighPriority() {
		return avgTimerReadHighPriority;
	}
	public void setAvgTimerReadHighPriority(long avgTimerReadHighPriority) {
		this.avgTimerReadHighPriority = avgTimerReadHighPriority;
	}
	public long getMaxTimerReadHighPriority() {
		return maxTimerReadHighPriority;
	}
	public void setMaxTimerReadHighPriority(long maxTimerReadHighPriority) {
		this.maxTimerReadHighPriority = maxTimerReadHighPriority;
	}
	public long getCountReadNoInsert() {
		return countReadNoInsert;
	}
	public void setCountReadNoInsert(long countReadNoInsert) {
		this.countReadNoInsert = countReadNoInsert;
	}
	public long getSumTimerReadNoInsert() {
		return sumTimerReadNoInsert;
	}
	public void setSumTimerReadNoInsert(long sumTimerReadNoInsert) {
		this.sumTimerReadNoInsert = sumTimerReadNoInsert;
	}
	public long getMinTimerReadNoInsert() {
		return minTimerReadNoInsert;
	}
	public void setMinTimerReadNoInsert(long minTimerReadNoInsert) {
		this.minTimerReadNoInsert = minTimerReadNoInsert;
	}
	public long getAvgTimerReadNoInsert() {
		return avgTimerReadNoInsert;
	}
	public void setAvgTimerReadNoInsert(long avgTimerReadNoInsert) {
		this.avgTimerReadNoInsert = avgTimerReadNoInsert;
	}
	public long getMaxTimerReadNoInsert() {
		return maxTimerReadNoInsert;
	}
	public void setMaxTimerReadNoInsert(long maxTimerReadNoInsert) {
		this.maxTimerReadNoInsert = maxTimerReadNoInsert;
	}
	public long getCountReadExternal() {
		return countReadExternal;
	}
	public void setCountReadExternal(long countReadExternal) {
		this.countReadExternal = countReadExternal;
	}
	public long getSumTimerReadExternal() {
		return sumTimerReadExternal;
	}
	public void setSumTimerReadExternal(long sumTimerReadExternal) {
		this.sumTimerReadExternal = sumTimerReadExternal;
	}
	public long getMinTimerReadExternal() {
		return minTimerReadExternal;
	}
	public void setMinTimerReadExternal(long minTimerReadExternal) {
		this.minTimerReadExternal = minTimerReadExternal;
	}
	public long getAvgTimerReadExternal() {
		return avgTimerReadExternal;
	}
	public void setAvgTimerReadExternal(long avgTimerReadExternal) {
		this.avgTimerReadExternal = avgTimerReadExternal;
	}
	public long getMaxTimerReadExternal() {
		return maxTimerReadExternal;
	}
	public void setMaxTimerReadExternal(long maxTimerReadExternal) {
		this.maxTimerReadExternal = maxTimerReadExternal;
	}
	public long getCountWriteAllowWrite() {
		return countWriteAllowWrite;
	}
	public void setCountWriteAllowWrite(long countWriteAllowWrite) {
		this.countWriteAllowWrite = countWriteAllowWrite;
	}
	public long getSumTimerWriteAllowWrite() {
		return sumTimerWriteAllowWrite;
	}
	public void setSumTimerWriteAllowWrite(long sumTimerWriteAllowWrite) {
		this.sumTimerWriteAllowWrite = sumTimerWriteAllowWrite;
	}
	public long getMinTimerWriteAllowWrite() {
		return minTimerWriteAllowWrite;
	}
	public void setMinTimerWriteAllowWrite(long minTimerWriteAllowWrite) {
		this.minTimerWriteAllowWrite = minTimerWriteAllowWrite;
	}
	public long getAvgTimerWriteAllowWrite() {
		return avgTimerWriteAllowWrite;
	}
	public void setAvgTimerWriteAllowWrite(long avgTimerWriteAllowWrite) {
		this.avgTimerWriteAllowWrite = avgTimerWriteAllowWrite;
	}
	public long getMaxTimerWriteAllowWrite() {
		return maxTimerWriteAllowWrite;
	}
	public void setMaxTimerWriteAllowWrite(long maxTimerWriteAllowWrite) {
		this.maxTimerWriteAllowWrite = maxTimerWriteAllowWrite;
	}
	public long getCountWriteConcurrentInsert() {
		return countWriteConcurrentInsert;
	}
	public void setCountWriteConcurrentInsert(long countWriteConcurrentInsert) {
		this.countWriteConcurrentInsert = countWriteConcurrentInsert;
	}
	public long getSumTimerWriteConcurrentInsert() {
		return sumTimerWriteConcurrentInsert;
	}
	public void setSumTimerWriteConcurrentInsert(long sumTimerWriteConcurrentInsert) {
		this.sumTimerWriteConcurrentInsert = sumTimerWriteConcurrentInsert;
	}
	public long getMinTimerWriteConcurrentInsert() {
		return minTimerWriteConcurrentInsert;
	}
	public void setMinTimerWriteConcurrentInsert(long minTimerWriteConcurrentInsert) {
		this.minTimerWriteConcurrentInsert = minTimerWriteConcurrentInsert;
	}
	public long getAvgTimerWriteConcurrentInsert() {
		return avgTimerWriteConcurrentInsert;
	}
	public void setAvgTimerWriteConcurrentInsert(long avgTimerWriteConcurrentInsert) {
		this.avgTimerWriteConcurrentInsert = avgTimerWriteConcurrentInsert;
	}
	public long getMaxTimerWriteConcurrentInsert() {
		return maxTimerWriteConcurrentInsert;
	}
	public void setMaxTimerWriteConcurrentInsert(long maxTimerWriteConcurrentInsert) {
		this.maxTimerWriteConcurrentInsert = maxTimerWriteConcurrentInsert;
	}
	public long getCountWriteDelayed() {
		return countWriteDelayed;
	}
	public void setCountWriteDelayed(long countWriteDelayed) {
		this.countWriteDelayed = countWriteDelayed;
	}
	public long getSumTimerWriteDelayed() {
		return sumTimerWriteDelayed;
	}
	public void setSumTimerWriteDelayed(long sumTimerWriteDelayed) {
		this.sumTimerWriteDelayed = sumTimerWriteDelayed;
	}
	public long getMinTimerWriteDelayed() {
		return minTimerWriteDelayed;
	}
	public void setMinTimerWriteDelayed(long minTimerWriteDelayed) {
		this.minTimerWriteDelayed = minTimerWriteDelayed;
	}
	public long getAvgTimerWriteDelayed() {
		return avgTimerWriteDelayed;
	}
	public void setAvgTimerWriteDelayed(long avgTimerWriteDelayed) {
		this.avgTimerWriteDelayed = avgTimerWriteDelayed;
	}
	public long getMaxTimerWriteDelayed() {
		return maxTimerWriteDelayed;
	}
	public void setMaxTimerWriteDelayed(long maxTimerWriteDelayed) {
		this.maxTimerWriteDelayed = maxTimerWriteDelayed;
	}
	public long getCountWriteLowPriority() {
		return countWriteLowPriority;
	}
	public void setCountWriteLowPriority(long countWriteLowPriority) {
		this.countWriteLowPriority = countWriteLowPriority;
	}
	public long getSumTimerWriteLowPriority() {
		return sumTimerWriteLowPriority;
	}
	public void setSumTimerWriteLowPriority(long sumTimerWriteLowPriority) {
		this.sumTimerWriteLowPriority = sumTimerWriteLowPriority;
	}
	public long getMinTimerWriteLowPriority() {
		return minTimerWriteLowPriority;
	}
	public void setMinTimerWriteLowPriority(long minTimerWriteLowPriority) {
		this.minTimerWriteLowPriority = minTimerWriteLowPriority;
	}
	public long getAvgTimerWriteLowPriority() {
		return avgTimerWriteLowPriority;
	}
	public void setAvgTimerWriteLowPriority(long avgTimerWriteLowPriority) {
		this.avgTimerWriteLowPriority = avgTimerWriteLowPriority;
	}
	public long getMaxTimerWriteLowPriority() {
		return maxTimerWriteLowPriority;
	}
	public void setMaxTimerWriteLowPriority(long maxTimerWriteLowPriority) {
		this.maxTimerWriteLowPriority = maxTimerWriteLowPriority;
	}
	public long getCountWriteNormal() {
		return countWriteNormal;
	}
	public void setCountWriteNormal(long countWriteNormal) {
		this.countWriteNormal = countWriteNormal;
	}
	public long getSumTimerWriteNormal() {
		return sumTimerWriteNormal;
	}
	public void setSumTimerWriteNormal(long sumTimerWriteNormal) {
		this.sumTimerWriteNormal = sumTimerWriteNormal;
	}
	public long getMinTimerWriteNormal() {
		return minTimerWriteNormal;
	}
	public void setMinTimerWriteNormal(long minTimerWriteNormal) {
		this.minTimerWriteNormal = minTimerWriteNormal;
	}
	public long getAvgTimerWriteNormal() {
		return avgTimerWriteNormal;
	}
	public void setAvgTimerWriteNormal(long avgTimerWriteNormal) {
		this.avgTimerWriteNormal = avgTimerWriteNormal;
	}
	public long getMaxTimerWriteNormal() {
		return maxTimerWriteNormal;
	}
	public void setMaxTimerWriteNormal(long maxTimerWriteNormal) {
		this.maxTimerWriteNormal = maxTimerWriteNormal;
	}
	public long getCountWriteExternal() {
		return countWriteExternal;
	}
	public void setCountWriteExternal(long countWriteExternal) {
		this.countWriteExternal = countWriteExternal;
	}
	public long getSumTimerWriteExternal() {
		return sumTimerWriteExternal;
	}
	public void setSumTimerWriteExternal(long sumTimerWriteExternal) {
		this.sumTimerWriteExternal = sumTimerWriteExternal;
	}
	public long getMinTimerWriteExternal() {
		return minTimerWriteExternal;
	}
	public void setMinTimerWriteExternal(long minTimerWriteExternal) {
		this.minTimerWriteExternal = minTimerWriteExternal;
	}
	public long getAvgTimerWriteExternal() {
		return avgTimerWriteExternal;
	}
	public void setAvgTimerWriteExternal(long avgTimerWriteExternal) {
		this.avgTimerWriteExternal = avgTimerWriteExternal;
	}
	public long getMaxTimerWriteExternal() {
		return maxTimerWriteExternal;
	}
	public void setMaxTimerWriteExternal(long maxTimerWriteExternal) {
		this.maxTimerWriteExternal = maxTimerWriteExternal;
	}
}
