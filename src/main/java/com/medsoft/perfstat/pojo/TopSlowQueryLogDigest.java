package com.medsoft.perfstat.pojo;

public class TopSlowQueryLogDigest extends SlowQueryLogDigest {
	private int totalResponseTimePct;
	private double avgResponseTime;
	private long avgRowsExamined;
	private long avgRowsEffected;
	public int getTotalResponseTimePct() {
		return totalResponseTimePct;
	}
	public void setTotalResponseTimePct(int totalResponseTimePct) {
		this.totalResponseTimePct = totalResponseTimePct;
	}
	public double getAvgResponseTime() {
		return avgResponseTime;
	}
	public void setAvgResponseTime(double avgResponseTime) {
		this.avgResponseTime = avgResponseTime;
	}
	public long getAvgRowsExamined() {
		return avgRowsExamined;
	}
	public void setAvgRowsExamined(long avgRowsExamined) {
		this.avgRowsExamined = avgRowsExamined;
	}
	public long getAvgRowsEffected() {
		return avgRowsEffected;
	}
	public void setAvgRowsEffected(long avgRowsEffected) {
		this.avgRowsEffected = avgRowsEffected;
	}
}