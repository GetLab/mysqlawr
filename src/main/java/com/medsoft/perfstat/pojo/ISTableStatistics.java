/**   
* @Title: ISTableStatistics.java 
* @Package com.medsoft.perfstat.pojo 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午9:42:07 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

/**
 * @author zjhua
 *
 */
public class ISTableStatistics extends BasePojo {
	private String tableSchema;
	private String tableName;
	private long rowsRead;
	private long rowsChanged;
	private long rowsChangedXIndexes;
	public String getTableSchema() {
		return tableSchema;
	}
	public void setTableSchema(String tableSchema) {
		this.tableSchema = tableSchema;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public long getRowsRead() {
		return rowsRead;
	}
	public void setRowsRead(long rowsRead) {
		this.rowsRead = rowsRead;
	}
	public long getRowsChanged() {
		return rowsChanged;
	}
	public void setRowsChanged(long rowsChanged) {
		this.rowsChanged = rowsChanged;
	}
	public long getRowsChangedXIndexes() {
		return rowsChangedXIndexes;
	}
	public void setRowsChangedXIndexes(long rowsChangedXIndexes) {
		this.rowsChangedXIndexes = rowsChangedXIndexes;
	}
}
