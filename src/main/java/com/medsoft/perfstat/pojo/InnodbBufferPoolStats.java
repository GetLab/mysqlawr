package com.medsoft.perfstat.pojo;

public class InnodbBufferPoolStats {
	private long freeBuffers;
	private long databasePages;
	private long oldDatabasePages;
	public long getFreeBuffers() {
		return freeBuffers;
	}
	public void setFreeBuffers(long freeBuffers) {
		this.freeBuffers = freeBuffers;
	}
	public long getDatabasePages() {
		return databasePages;
	}
	public void setDatabasePages(long databasePages) {
		this.databasePages = databasePages;
	}
	public long getOldDatabasePages() {
		return oldDatabasePages;
	}
	public void setOldDatabasePages(long oldDatabasePages) {
		this.oldDatabasePages = oldDatabasePages;
	}
}
