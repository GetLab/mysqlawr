/**   
* @Title: ISGlobalVariable.java 
* @Package com.medsoft.perfstat.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午9:34:51 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zjhua
 *
 */
public class ISGlobalVariableChange extends AppSnap {
	private static final Map<String,String> variableMap = new HashMap<String,String>();
	static {
		variableMap.put("SOCKET".toUpperCase(), "/tmp/mysql.sock");
		variableMap.put("BASEDIR".toUpperCase(), "/usr/local/mysql或其指向的路径");
		variableMap.put("BINLOG_ROWS_QUERY_LOG_EVENTS".toUpperCase(), "ON");
		variableMap.put("DATADIR".toUpperCase(), "basedir/data");
		variableMap.put("DEFAULT_STORAGE_ENGINE".toUpperCase(), "InnoDB");
		variableMap.put("DEFAULT_TMP_STORAGE_ENGINE".toUpperCase(), "MEMORY");
		variableMap.put("EVENT_SCHEDULER".toUpperCase(), "ON");
		variableMap.put("INNODB_VERSION".toUpperCase(), "5.6.30+/5.7.16+");
		variableMap.put("INNODB_USE_GLOBAL_FLUSH_LOG_AT_TRX_COMMIT".toUpperCase(), "0");
		variableMap.put("EXPIRE_LOGS_DAYS".toUpperCase(), "3-7,视具体情况");
		variableMap.put("GENERAL_LOG".toUpperCase(), "OFF");
		variableMap.put("INNODB_AUTOINC_LOCK_MODE".toUpperCase(), "2");
		variableMap.put("INNODB_FLUSH_LOG_AT_TRX_COMMIT".toUpperCase(), "1");
		variableMap.put("INNODB_LOG_FILES_IN_GROUP".toUpperCase(), "3");
		variableMap.put("INNODB_AUTOINC_LOCK_MODE".toUpperCase(), "2");
		variableMap.put("INNODB_PRINT_ALL_DEADLOCKS".toUpperCase(), "ON");
		variableMap.put("INNODB_ROLLBACK_ON_TIMEOUT".toUpperCase(), "ON");
		variableMap.put("LOG_BIN".toUpperCase(), "ON");
		variableMap.put("LOG_BIN_TRUST_FUNCTION_CREATORS".toUpperCase(), "ON");
		variableMap.put("LOG_SLAVE_UPDATES".toUpperCase(), "ON");
		variableMap.put("LOG_SLOW_VERBOSITY".toUpperCase(), "microtime,query_plan,innodb");
		variableMap.put("MASTER_INFO_REPOSITORY".toUpperCase(), "TABLE");
		variableMap.put("MAX_BINLOG_SIZE".toUpperCase(), "512M或1024M");
		variableMap.put("MAX_SLOWLOG_FILES".toUpperCase(), "10");
		variableMap.put("MAX_SLOWLOG_SIZE".toUpperCase(), "10M");
		variableMap.put("PID_FILE".toUpperCase(), "basedir/data/hostname.pid");
		variableMap.put("PROFILING".toUpperCase(), "ON");
		variableMap.put("QUERY_CACHE_TYPE".toUpperCase(), "OFF");
		variableMap.put("SYNC_BINLOG".toUpperCase(), "1");
		variableMap.put("VERSION_COMMENT".toUpperCase(), "Percona Server");
		variableMap.put("VERSION_COMPILE_MACHINE".toUpperCase(), "x86_64");
		variableMap.put("VERSION_COMMENT".toUpperCase(), "system jemalloc");
		
		variableMap.put("innodb_strict_mode".toUpperCase(), "true");
		variableMap.put("autocommit".toUpperCase(), "OFF");
		variableMap.put("TX_ISOLATION".toUpperCase(), "READ-COMMITTED");
		variableMap.put("lower_case_table_names".toUpperCase(), "ON");
		variableMap.put("log-bin".toUpperCase(), "mysql-bin");
		variableMap.put("innodb_kill_idle_transaction".toUpperCase(), "180");
		variableMap.put("binlog_format".toUpperCase(), "row");
		variableMap.put("innodb_lock_wait_timeout".toUpperCase(), "3");
		variableMap.put("performance_schema".toUpperCase(), "ON");
		variableMap.put("MAX_STATEMENT_TIME".toUpperCase(), "3");
		variableMap.put("innodb-file-per-table".toUpperCase(), "true");
		variableMap.put("innodb_buffer_pool_size".toUpperCase(), "16GB以下60%*物理内存，16GB以上75%*物理内存");
		variableMap.put("innodb_flush_method".toUpperCase(), "ALL_O_DIRECT");
		variableMap.put("innodb_buffer_pool_instances".toUpperCase(), "innodb_buffer_pool_size/1GB");
		variableMap.put("innodb_read_io_threads".toUpperCase(), "min(cpu核心数,16)");
		variableMap.put("innodb_write_io_threads".toUpperCase(), "min(cpu核心数,16)");
		variableMap.put("thread_handling".toUpperCase(), "pool-of-threads(仅percona/mariadb)");
		variableMap.put("thread_pool_size".toUpperCase(), "min(CPU数量,8)（仅percona/mariadb）");
		variableMap.put("thread_cache_size".toUpperCase(), "cpu数量*20 （仅percona/mariadb）");
		variableMap.put("innodb_log_file_size".toUpperCase(), "512M");
		variableMap.put("slow_query_log".toUpperCase(), "ON");
		variableMap.put("SLOW_QUERY_LOG_FILE".toUpperCase(), "basedir/data/slow-query.log");
		variableMap.put("long_query_time".toUpperCase(), "0.03");
		variableMap.put("userstat".toUpperCase(), "ON（percona/mariadb专有）");
		variableMap.put("innodb_show_locks_held".toUpperCase(), "100（percona/mariadb专有）");
		variableMap.put("innodb_show_verbose_locks".toUpperCase(), "1（percona/mariadb专有）");
		
		variableMap.put("CHARACTER_SET_CLIENT".toUpperCase(), "utf8");
		variableMap.put("CHARACTER_SET_CONNECTION".toUpperCase(), "utf8");
		variableMap.put("CHARACTER_SET_DATABASE".toUpperCase(), "utf8");
		variableMap.put("CHARACTER_SET_FILESYSTEM".toUpperCase(), "binary");
		variableMap.put("CHARACTER_SET_RESULTS".toUpperCase(), "utf8");
		variableMap.put("CHARACTER_SET_SERVER".toUpperCase(), "utf8");
		variableMap.put("CHARACTER_SET_SYSTEM".toUpperCase(), "utf8");
		variableMap.put("COLLATION_CONNECTION".toUpperCase(), "utf8_general_ci");
		variableMap.put("COLLATION_DATABASE".toUpperCase(), "utf8_general_ci");
		variableMap.put("COLLATION_SERVER".toUpperCase(), "utf8_general_ci");
	}
	private String variableName;
	private String preVariableValue;
	private String curVariableValue;
	private String isChanged;
	private String recommendValue;
	
	public String getPreVariableValue() {
		return preVariableValue;
	}
	public void setPreVariableValue(String preVariableValue) {
		this.preVariableValue = preVariableValue;
	}
	public String getIsChanged() {
		return preVariableValue.equals(curVariableValue) ? "是" : "否";
	}
	public void setIsChanged(String isChanged) {
		this.isChanged = isChanged;
	}
	public String getVariableName() {
		return variableName;
	}
	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}
	public String getRecommendValue() {
		return variableMap.get(variableName);
	}
	public void setRecommendValue(String recommendValue) {
		this.recommendValue = recommendValue;
	}
	public String getCurVariableValue() {
		return curVariableValue;
	}
	public void setCurVariableValue(String curVariableValue) {
		this.curVariableValue = curVariableValue;
	}
}
