/**   
* @Title: Top10ObjectIOWait.java 
* @Package com.medsoft.perfstat.pojo 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月16日 下午8:21:40 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

/**
 * @author zjhua
 *
 */
public class Top10ObjectIOWait {
	private String objectType;
	private String objectSchema;
	private String objectName;
	private String indexName; 
	private long countStar;
	private long sumTimerWait; //ms
	private int avgTimerWait; //ms
	private long countRead;
	private long sumTimerRead; //ms
	private int avgTimerRead; //ms
	private long countWrite;
	private long sumTimerWrite; //ms
	private int avgTimerWrite; //ms
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public String getObjectSchema() {
		return objectSchema;
	}
	public void setObjectSchema(String objectSchema) {
		this.objectSchema = objectSchema;
	}
	public String getObjectName() {
		return objectName;
	}
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	public String getIndexName() {
		return indexName;
	}
	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}
	public long getCountStar() {
		return countStar;
	}
	public void setCountStar(long countStar) {
		this.countStar = countStar;
	}
	public long getSumTimerWait() {
		return sumTimerWait;
	}
	public void setSumTimerWait(long sumTimerWait) {
		this.sumTimerWait = sumTimerWait;
	}
	public int getAvgTimerWait() {
		return avgTimerWait;
	}
	public void setAvgTimerWait(int avgTimerWait) {
		this.avgTimerWait = avgTimerWait;
	}
	public long getCountRead() {
		return countRead;
	}
	public void setCountRead(long countRead) {
		this.countRead = countRead;
	}
	public long getSumTimerRead() {
		return sumTimerRead;
	}
	public void setSumTimerRead(long sumTimerRead) {
		this.sumTimerRead = sumTimerRead;
	}
	public int getAvgTimerRead() {
		return avgTimerRead;
	}
	public void setAvgTimerRead(int avgTimerRead) {
		this.avgTimerRead = avgTimerRead;
	}
	public long getCountWrite() {
		return countWrite;
	}
	public void setCountWrite(long countWrite) {
		this.countWrite = countWrite;
	}
	public long getSumTimerWrite() {
		return sumTimerWrite;
	}
	public void setSumTimerWrite(long sumTimerWrite) {
		this.sumTimerWrite = sumTimerWrite;
	}
	public int getAvgTimerWrite() {
		return avgTimerWrite;
	}
	public void setAvgTimerWrite(int avgTimerWrite) {
		this.avgTimerWrite = avgTimerWrite;
	}
}
