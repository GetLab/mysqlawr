/**   
* @Title: PSEventsWaitsSummaryGlobalByEventName.java 
* @Package com.medsoft.perfstat.pojo 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午9:35:35 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

/**
 * @author zjhua
 *
 */
public class PSEventsWaitsSummaryGlobalByEventName extends AppSnap{
	private String eventName;
	private String countStar;
	private long sumTimerWait;
	private long minTimerWait;
	private long maxTimerWait;
	private long avgTimerWait;
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getCountStar() {
		return countStar;
	}
	public void setCountStar(String countStar) {
		this.countStar = countStar;
	}
	public long getSumTimerWait() {
		return sumTimerWait;
	}
	public void setSumTimerWait(long sumTimerWait) {
		this.sumTimerWait = sumTimerWait;
	}
	public long getMinTimerWait() {
		return minTimerWait;
	}
	public void setMinTimerWait(long minTimerWait) {
		this.minTimerWait = minTimerWait;
	}
	public long getMaxTimerWait() {
		return maxTimerWait;
	}
	public void setMaxTimerWait(long maxTimerWait) {
		this.maxTimerWait = maxTimerWait;
	}
	public long getAvgTimerWait() {
		return avgTimerWait;
	}
	public void setAvgTimerWait(long avgTimerWait) {
		this.avgTimerWait = avgTimerWait;
	}
}
