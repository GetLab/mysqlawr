package com.medsoft.perfstat.pojo;

public class Proc {
	private String db;
	private String name;
	private String comment;
	
	private String parentDb;
	private String parentName;
	
	public Proc(String db, String name) {
		super();
		this.db = db;
		this.name = name;
	}
	public Proc(String db, String name,String parentDb,String parentName) {
		super();
		this.db = db;
		this.name = name;
		this.parentDb = parentDb;
		this.parentName = parentName;
	}
	public String getDb() {
		return db;
	}
	public void setDb(String db) {
		this.db = db;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParentDb() {
		return parentDb;
	}
	public void setParentDb(String parentDb) {
		this.parentDb = parentDb;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	
	public String getParent() {
		return this.parentDb + "." + this.parentName;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
}
