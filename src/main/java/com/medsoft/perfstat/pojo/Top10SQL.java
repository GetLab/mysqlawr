/**   
* @Title: Top10SQL.java 
* @Package com.medsoft.perfstat.pojo 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月16日 下午8:19:30 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

/**
 * @author zjhua
 *
 */
public class Top10SQL {
	private String digest;
	private String digestText;
	private String schemaName;
	private long countStar;
	private long sumTimerWait; //ms
	private int avgTimerWait; //ms
	private long sumRowsExamined;
	private int avgRowsExamined;
	private double sumTimerWaitPct;
	private double sumRowsExaminedPct;
	private String lastSeen;
	
	public double getSumTimerWaitPct() {
		return sumTimerWaitPct;
	}
	public void setSumTimerWaitPct(double sumTimerWaitPct) {
		this.sumTimerWaitPct = sumTimerWaitPct;
	}
	public double getSumRowsExaminedPct() {
		return sumRowsExaminedPct;
	}
	public void setSumRowsExaminedPct(double sumRowsExaminedPct) {
		this.sumRowsExaminedPct = sumRowsExaminedPct;
	}
	public String getDigest() {
		return digest;
	}
	public void setDigest(String digest) {
		this.digest = digest;
	}
	public String getDigestText() {
		return digestText;
	}
	public void setDigestText(String digestText) {
		this.digestText = digestText;
	}
	public long getCountStar() {
		return countStar;
	}
	public void setCountStar(long countStar) {
		this.countStar = countStar;
	}
	public long getSumTimerWait() {
		return sumTimerWait;
	}
	public void setSumTimerWait(long sumTimerWait) {
		this.sumTimerWait = sumTimerWait;
	}
	public int getAvgTimerWait() {
		return avgTimerWait;
	}
	public void setAvgTimerWait(int avgTimerWait) {
		this.avgTimerWait = avgTimerWait;
	}
	public long getSumRowsExamined() {
		return sumRowsExamined;
	}
	public void setSumRowsExamined(long sumRowsExamined) {
		this.sumRowsExamined = sumRowsExamined;
	}
	public int getAvgRowsExamined() {
		return avgRowsExamined;
	}
	public void setAvgRowsExamined(int avgRowsExamined) {
		this.avgRowsExamined = avgRowsExamined;
	}
	public String getLastSeen() {
		return lastSeen;
	}
	public void setLastSeen(String lastSeen) {
		this.lastSeen = lastSeen;
	}
	public String getSchemaName() {
		return schemaName;
	}
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}
}