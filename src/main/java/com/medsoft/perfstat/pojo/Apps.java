/**   
* @Title: Apps.java 
* @Package com.medsoft.perfstat.pojo 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午9:49:04 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

/**
 * @author zjhua
 *
 */
public class Apps extends BasePojo {
	private String hostname;
	private int mapPort;
	private int port;
	private String appname;
	private String ver;
	
	private String mysqlUsername;
	private String mysqlPassword;
	public String getMysqlUsername() {
		return mysqlUsername;
	}
	public void setMysqlUsername(String mysqlUsername) {
		this.mysqlUsername = mysqlUsername;
	}
	public String getMysqlPassword() {
		return mysqlPassword;
	}
	public void setMysqlPassword(String mysqlPassword) {
		this.mysqlPassword = mysqlPassword;
	}
	private String sshUsername;
	private String sshPassword;
	private int sshPort;
	
	private int snapInterval;
	
	public String getSshUsername() {
		return sshUsername;
	}
	public void setSshUsername(String sshUsername) {
		this.sshUsername = sshUsername;
	}
	public String getSshPassword() {
		return sshPassword;
	}
	public void setSshPassword(String sshPassword) {
		this.sshPassword = sshPassword;
	}
	public int getSshPort() {
		return sshPort;
	}
	public void setSshPort(int sshPort) {
		this.sshPort = sshPort;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getAppname() {
		return appname;
	}
	public void setAppname(String appname) {
		this.appname = appname;
	}
	public String getVer() {
		return ver;
	}
	public void setVer(String ver) {
		this.ver = ver;
	}
	@Override
	public String toString() {
		return "Apps [hostname=" + hostname + ", port=" + port + ", appname="
				+ appname + ", ver=" + ver + ", mysqlUsername=" + mysqlUsername
				+ ", mysqlPassword=" + mysqlPassword + ", sshUsername="
				+ sshUsername + ", sshPassword=" + sshPassword + ", sshPort="
				+ sshPort + "]";
	}
	public int getSnapInterval() {
		return snapInterval;
	}
	public void setSnapInterval(int snapInterval) {
		this.snapInterval = snapInterval;
	}
	public int getMapPort() {
		return mapPort;
	}
	public void setMapPort(int mapPort) {
		this.mapPort = mapPort;
	}
}
