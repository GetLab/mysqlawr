/**   
* @Title: PSEventsStatementsSummaryByDigest.java 
* @Package com.medsoft.perfstat.pojo 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午9:39:53 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

/**
 * @author zjhua
 *
 */
public class PSEventsStatementsSummaryByDigest extends AppSnap {
	private String schemaName;
	private String digest;
	private String digestText;
	private long countStar;
	private long sumTimerWait;
	private long minTimerWait;
	private long avgTimerWait;
	private long maxTimerWait;
	private long sumLockTime;
	private long sumErrors;
	private long sumWarnings;
	private long sumRowsAffected;
	private long sumRowsSent;
	private long sumRowsExamined;
	private long sumCreatedTmpDiskTables;
	private long sumCreatedTmpTables;
	private long sumSelectFullJoin;
	private long sumSelectFullRangeJoin;
	private long sumSelectRange;
	private long sumSelectRangeCheck;
	private long sumSelectScan;
	private long sumSortMergePasses;
	private long sumSortRange;
	private long sumSortRows;
	private long sumSortScan;
	private long sumNoIndexUsed;
	private long sumNoGoodIndexUsed;
	private String firstSeen;
	private String lastSeen;
	public String getSchemaName() {
		return schemaName;
	}
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}
	public String getDigest() {
		return digest;
	}
	public void setDigest(String digest) {
		this.digest = digest;
	}
	public String getDigestText() {
		return digestText;
	}
	public void setDigestText(String digestText) {
		this.digestText = digestText;
	}
	public long getCountStar() {
		return countStar;
	}
	public void setCountStar(long countStar) {
		this.countStar = countStar;
	}
	public long getSumTimerWait() {
		return sumTimerWait;
	}
	public void setSumTimerWait(long sumTimerWait) {
		this.sumTimerWait = sumTimerWait;
	}
	public long getMinTimerWait() {
		return minTimerWait;
	}
	public void setMinTimerWait(long minTimerWait) {
		this.minTimerWait = minTimerWait;
	}
	public long getAvgTimerWait() {
		return avgTimerWait;
	}
	public void setAvgTimerWait(long avgTimerWait) {
		this.avgTimerWait = avgTimerWait;
	}
	public long getMaxTimerWait() {
		return maxTimerWait;
	}
	public void setMaxTimerWait(long maxTimerWait) {
		this.maxTimerWait = maxTimerWait;
	}
	public long getSumLockTime() {
		return sumLockTime;
	}
	public void setSumLockTime(long sumLockTime) {
		this.sumLockTime = sumLockTime;
	}
	public long getSumErrors() {
		return sumErrors;
	}
	public void setSumErrors(long sumErrors) {
		this.sumErrors = sumErrors;
	}
	public long getSumWarnings() {
		return sumWarnings;
	}
	public void setSumWarnings(long sumWarnings) {
		this.sumWarnings = sumWarnings;
	}
	public long getSumRowsAffected() {
		return sumRowsAffected;
	}
	public void setSumRowsEffected(long sumRowsAffected) {
		this.sumRowsAffected = sumRowsAffected;
	}
	public long getSumRowsSent() {
		return sumRowsSent;
	}
	public void setSumRowsSent(long sumRowsSent) {
		this.sumRowsSent = sumRowsSent;
	}
	public long getSumRowsExamined() {
		return sumRowsExamined;
	}
	public void setSumRowsExamined(long sumRowsExamined) {
		this.sumRowsExamined = sumRowsExamined;
	}
	public long getSumCreatedTmpDiskTables() {
		return sumCreatedTmpDiskTables;
	}
	public void setSumCreatedTmpDiskTables(long sumCreatedTmpDiskTables) {
		this.sumCreatedTmpDiskTables = sumCreatedTmpDiskTables;
	}
	public long getSumCreatedTmpTables() {
		return sumCreatedTmpTables;
	}
	public void setSumCreatedTmpTables(long sumCreatedTmpTables) {
		this.sumCreatedTmpTables = sumCreatedTmpTables;
	}
	public long getSumSelectRange() {
		return sumSelectRange;
	}
	public void setSumSelectRange(long sumSelectRange) {
		this.sumSelectRange = sumSelectRange;
	}
	public long getSumSelectRangeCheck() {
		return sumSelectRangeCheck;
	}
	public void setSumSelectRangeCheck(long sumSelectRangeCheck) {
		this.sumSelectRangeCheck = sumSelectRangeCheck;
	}
	public long getSumSelectScan() {
		return sumSelectScan;
	}
	public void setSumSelectScan(long sumSelectScan) {
		this.sumSelectScan = sumSelectScan;
	}
	public long getSumSortMergePasses() {
		return sumSortMergePasses;
	}
	public void setSumSortMergePasses(long sumSortMergePasses) {
		this.sumSortMergePasses = sumSortMergePasses;
	}
	public long getSumSortRange() {
		return sumSortRange;
	}
	public void setSumSortRange(long sumSortRange) {
		this.sumSortRange = sumSortRange;
	}
	public long getSumSortRows() {
		return sumSortRows;
	}
	public void setSumSortRows(long sumSortRows) {
		this.sumSortRows = sumSortRows;
	}
	public long getSumSortScan() {
		return sumSortScan;
	}
	public void setSumSortScan(long sumSortScan) {
		this.sumSortScan = sumSortScan;
	}
	public long getSumNoIndexUsed() {
		return sumNoIndexUsed;
	}
	public void setSumNoIndexUsed(long sumNoIndexUsed) {
		this.sumNoIndexUsed = sumNoIndexUsed;
	}
	public long getSumNoGoodIndexUsed() {
		return sumNoGoodIndexUsed;
	}
	public void setSumNoGoodIndexUsed(long sumNoGoodIndexUsed) {
		this.sumNoGoodIndexUsed = sumNoGoodIndexUsed;
	}
	public String getFirstSeen() {
		return firstSeen;
	}
	public void setFirstSeen(String firstSeen) {
		this.firstSeen = firstSeen;
	}
	public String getLastSeen() {
		return lastSeen;
	}
	public void setLastSeen(String lastSeen) {
		this.lastSeen = lastSeen;
	}
	public long getSumSelectFullJoin() {
		return sumSelectFullJoin;
	}
	public void setSumSelectFullJoin(long sumSelectFullJoin) {
		this.sumSelectFullJoin = sumSelectFullJoin;
	}
	public long getSumSelectFullRangeJoin() {
		return sumSelectFullRangeJoin;
	}
	public void setSumSelectFullRangeJoin(long sumSelectFullRangeJoin) {
		this.sumSelectFullRangeJoin = sumSelectFullRangeJoin;
	}
}
