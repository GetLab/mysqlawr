/**   
* @Title: ISGlobalStatus.java 
* @Package com.medsoft.perfstat.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午9:31:59 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;


/**
 * @author zjhua
 *
 */
public class ISGlobalStatus extends AppSnap {
	private String variableName;
	private String variableValue;
	public String getVariableName() {
		return variableName;
	}
	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}
	public String getVariableValue() {
		return variableValue;
	}
	public void setVariableValue(String variableValue) {
		this.variableValue = variableValue;
	}
	
	public void put(String variableName,String variableValue) {
		this.variableName = variableName;
		this.variableValue = variableValue;
	}
}
