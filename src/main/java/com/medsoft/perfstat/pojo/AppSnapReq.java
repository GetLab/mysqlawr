/**   
* @Title: AppSnapReq.java 
* @Package com.medsoft.perfstat.pojo 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月17日 上午10:28:39 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

/**
 * @author zjhua
 *
 */
public class AppSnapReq extends BasePojo {
	private String appname;
	private String beginSnapId;
	private String endSnapId;
	private String beginSnapTime;
	private String endSnapTime;
	private String elapsed;
	
	public String getBeginSnapTime() {
		return beginSnapTime;
	}
	public void setBeginSnapTime(String beginSnapTime) {
		this.beginSnapTime = beginSnapTime;
	}
	public String getEndSnapTime() {
		return endSnapTime;
	}
	public void setEndSnapTime(String endSnapTime) {
		this.endSnapTime = endSnapTime;
	}
	public String getAppname() {
		return appname;
	}
	public void setAppname(String appname) {
		this.appname = appname;
	}
	public String getBeginSnapId() {
		return beginSnapId;
	}
	public void setBeginSnapId(String beginSnapId) {
		this.beginSnapId = beginSnapId;
	}
	public String getEndSnapId() {
		return endSnapId;
	}
	public void setEndSnapId(String endSnapId) {
		this.endSnapId = endSnapId;
	}
	public String getElapsed() {
		return elapsed;
	}
	public void setElapsed(String elapsed) {
		this.elapsed = elapsed;
	}
}
