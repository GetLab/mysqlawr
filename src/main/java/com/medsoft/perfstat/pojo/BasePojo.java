/**   
* @Title: BasePojo.java 
* @Package com.medsoft.logpool.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午5:33:14 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

/**
 * @author zjhua
 *
 */
public class BasePojo {
	private String hasHidden;

	public String getHasHidden() {
		return hasHidden;
	}

	public void setHasHidden(String hasHidden) {
		this.hasHidden = hasHidden;
	}
}
