/**   
* @Title: AppSnap.java 
* @Package com.medsoft.perfstat.core 
* @Description: TODO(用一句话描述该文件做什么) 
* @author zjhua@hundsun.com   
* @date 2016年7月13日 下午9:31:32 
* @version V1.0   
*/ 
package com.medsoft.perfstat.pojo;

import org.apache.commons.lang3.StringUtils;


/**
 * @author zjhua
 *
 */
public class AppSnap extends BasePojo {
	private String hostname;
	private String appname;
	private int snapId;
	private String logTime;
	private String execResultInfo = "";
	
	private String mysqlStartTime;
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public String getAppname() {
		return appname;
	}
	public void setAppname(String appname) {
		this.appname = appname;
	}
	public int getSnapId() {
		return snapId;
	}
	public void setSnapId(int snapId) {
		this.snapId = snapId;
	}
	public String getLogTime() {
		return logTime;
	}
	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	public String getExecResultInfo() {
		return StringUtils.isEmpty(execResultInfo) ? "执行成功！" : this.execResultInfo;
	}
	public void setExecResultInfo(String execResultInfo) {
		this.execResultInfo += execResultInfo;
	}
	
	public void trim256() {
		this.execResultInfo = this.execResultInfo.substring(0, Math.min(this.execResultInfo.length(),256));
	}
	public String getMysqlStartTime() {
		return mysqlStartTime.replaceAll("_", "-");
	}
	public void setMysqlStartTime(String mysqlStartTime) {
		this.mysqlStartTime = mysqlStartTime;
	}
}
