package com.medsoft.perfstat.pojo;

public class SlowQueryLogDigest extends AppSnap {
	private String queryId;
	private double totalResponseTime;
	private long calls;
	private String abbrSql;
	private String fullSql;
	private long totalRowsExamined;
	private long totalRowsEffected;
	public String getQueryId() {
		return queryId;
	}
	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}
	public double getTotalResponseTime() {
		return totalResponseTime;
	}
	public void setTotalResponseTime(double totalResponseTime) {
		this.totalResponseTime = totalResponseTime;
	}
	public long getCalls() {
		return calls;
	}
	public void setCalls(long calls) {
		this.calls = calls;
	}
	public String getAbbrSql() {
		return abbrSql;
	}
	public void setAbbrSql(String abbrSql) {
		this.abbrSql = abbrSql;
	}
	public String getFullSql() {
		return fullSql;
	}
	public void setFullSql(String fullSql) {
		this.fullSql = fullSql;
	}
	public long getTotalRowsExamined() {
		return totalRowsExamined;
	}
	public void setTotalRowsExamined(long totalRowsExamined) {
		this.totalRowsExamined = totalRowsExamined;
	}
	public long getTotalRowsEffected() {
		return totalRowsEffected;
	}
	public void setTotalRowsEffected(long totalRowsEffected) {
		this.totalRowsEffected = totalRowsEffected;
	}
}