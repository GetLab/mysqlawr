CREATE database perf_stat default character set gbk;
use perf_stat;

CREATE table apps (
hostname varchar(64),
port int,
map_port int,
appname varchar(64) primary key,
ver varchar(64),
mysql_username varchar(32),
mysql_password varchar(64),
ssh_username varchar(32),
ssh_password varchar(64),
ssh_port int,
snap_interval int);

CREATE table app_snaps (hostname varchar(64),appname varchar(64),snap_id int,log_time datetime,exec_result_info varchar(256)); -- 保留7天自动删除
CREATE index idx_app_snaps_appname on app_snaps(appname);

CREATE table is_global_status(hostname varchar(64),appname varchar(64),log_time datetime,snap_id int,
variable_name varchar(64),
variable_value varchar(64)); -- 启动以来全量
CREATE index idx_global_status_app_snap on is_global_status(appname,snap_id);
CREATE index idx_global_status_variable_name on is_global_status(variable_name,appname,snap_id);
CREATE index idx_global_status_log_time on is_global_status(log_time);
-- mysql进程的cpu时间以及物理内存存储存储为变量名cpu_time, memory_used。

CREATE table is_global_variables(hostname varchar(64),appname varchar(64),log_time datetime,snap_id int,
variable_name varchar(64),
variable_value varchar(64));  -- 启动以来全量
CREATE index idx_global_VARIABLES_app_snap on is_global_variables(appname,snap_id);
CREATE index idx_global_VARIABLES_variable_name on is_global_variables(variable_name,appname,snap_id);
CREATE index idx_global_VARIABLES_log_time on is_global_variables(log_time);

CREATE table ps_events_waits_summary_global_by_event_name 
(
   hostname varchar(64),appname varchar(64),log_time datetime,snap_id int,
  `EVENT_NAME` varchar(128) NOT NULL,
  `COUNT_STAR` bigint(20) unsigned NOT NULL,
  `SUM_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `MIN_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `AVG_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `MAX_TIMER_WAIT` bigint(20) unsigned NOT NULL
);
CREATE index idx_ewsgben_app_snap on ps_events_waits_summary_global_by_event_name(appname,snap_id);

CREATE table ps_table_io_waits_summary_by_table 
(
    hostname varchar(64),appname varchar(64),log_time datetime,snap_id int,
  `OBJECT_TYPE` varchar(64) DEFAULT NULL,
  `OBJECT_SCHEMA` varchar(64) DEFAULT NULL,
  `OBJECT_NAME` varchar(64) DEFAULT NULL,
  `COUNT_STAR` bigint(20) unsigned NOT NULL,
  `SUM_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `MIN_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `AVG_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `MAX_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `COUNT_READ` bigint(20) unsigned NOT NULL,
  `SUM_TIMER_READ` bigint(20) unsigned NOT NULL,
  `MIN_TIMER_READ` bigint(20) unsigned NOT NULL,
  `AVG_TIMER_READ` bigint(20) unsigned NOT NULL,
  `MAX_TIMER_READ` bigint(20) unsigned NOT NULL,
  `COUNT_WRITE` bigint(20) unsigned NOT NULL,
  `SUM_TIMER_WRITE` bigint(20) unsigned NOT NULL,
  `MIN_TIMER_WRITE` bigint(20) unsigned NOT NULL,
  `AVG_TIMER_WRITE` bigint(20) unsigned NOT NULL,
  `MAX_TIMER_WRITE` bigint(20) unsigned NOT NULL,
  `COUNT_FETCH` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_FETCH` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_FETCH` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_FETCH` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_FETCH` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_UPDATE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_UPDATE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_UPDATE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_UPDATE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_UPDATE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_DELETE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_DELETE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_DELETE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_DELETE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_DELETE` bigint(20) unsigned NOT NULL  DEFAULT '0'
);
CREATE index idx_tiwsbt_app_snap on ps_table_io_waits_summary_by_table(appname,snap_id);
CREATE index idx_tiwsbt_log_time on ps_table_io_waits_summary_by_table(log_time);

CREATE table ps_table_lock_waits_summary_by_table 
(
    hostname varchar(64),appname varchar(64),log_time datetime,snap_id int,
  `OBJECT_TYPE` varchar(64) DEFAULT NULL,
  `OBJECT_SCHEMA` varchar(64) DEFAULT NULL,
  `OBJECT_NAME` varchar(64) DEFAULT NULL,
  `COUNT_STAR` bigint(20) unsigned NOT NULL,
  `SUM_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `MIN_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `AVG_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `MAX_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `COUNT_READ` bigint(20) unsigned NOT NULL,
  `SUM_TIMER_READ` bigint(20) unsigned NOT NULL,
  `MIN_TIMER_READ` bigint(20) unsigned NOT NULL,
  `AVG_TIMER_READ` bigint(20) unsigned NOT NULL,
  `MAX_TIMER_READ` bigint(20) unsigned NOT NULL,
  `COUNT_WRITE` bigint(20) unsigned NOT NULL,
  `SUM_TIMER_WRITE` bigint(20) unsigned NOT NULL,
  `MIN_TIMER_WRITE` bigint(20) unsigned NOT NULL,
  `AVG_TIMER_WRITE` bigint(20) unsigned NOT NULL,
  `MAX_TIMER_WRITE` bigint(20) unsigned NOT NULL,
  `COUNT_READ_NORMAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_READ_NORMAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_READ_NORMAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_READ_NORMAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_READ_NORMAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_READ_WITH_SHARED_LOCKS` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_READ_WITH_SHARED_LOCKS` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_READ_WITH_SHARED_LOCKS` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_READ_WITH_SHARED_LOCKS` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_READ_WITH_SHARED_LOCKS` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_READ_HIGH_PRIORITY` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_READ_HIGH_PRIORITY` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_READ_HIGH_PRIORITY` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_READ_HIGH_PRIORITY` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_READ_HIGH_PRIORITY` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_READ_NO_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_READ_NO_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_READ_NO_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_READ_NO_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_READ_NO_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_READ_EXTERNAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_READ_EXTERNAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_READ_EXTERNAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_READ_EXTERNAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_READ_EXTERNAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_WRITE_ALLOW_WRITE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_WRITE_ALLOW_WRITE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_WRITE_ALLOW_WRITE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_WRITE_ALLOW_WRITE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_WRITE_ALLOW_WRITE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_WRITE_CONCURRENT_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_WRITE_CONCURRENT_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_WRITE_CONCURRENT_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_WRITE_CONCURRENT_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_WRITE_CONCURRENT_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_WRITE_DELAYED` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_WRITE_DELAYED` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_WRITE_DELAYED` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_WRITE_DELAYED` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_WRITE_DELAYED` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_WRITE_LOW_PRIORITY` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_WRITE_LOW_PRIORITY` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_WRITE_LOW_PRIORITY` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_WRITE_LOW_PRIORITY` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_WRITE_LOW_PRIORITY` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_WRITE_NORMAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_WRITE_NORMAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_WRITE_NORMAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_WRITE_NORMAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_WRITE_NORMAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_WRITE_EXTERNAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_WRITE_EXTERNAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_WRITE_EXTERNAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_WRITE_EXTERNAL` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_WRITE_EXTERNAL` bigint(20) unsigned NOT NULL  DEFAULT '0'
);
CREATE index idx_tlwsbt_app_snap on ps_table_lock_waits_summary_by_table(appname,snap_id);
CREATE index idx_tlwsbt_log_time on ps_table_lock_waits_summary_by_table(log_time);


CREATE table ps_table_io_waits_summary_by_index_usage 
(
    hostname varchar(64),appname varchar(64),log_time datetime,snap_id int,
  `OBJECT_TYPE` varchar(64) DEFAULT NULL,
  `OBJECT_SCHEMA` varchar(64) DEFAULT NULL,
  `OBJECT_NAME` varchar(64) DEFAULT NULL,
  `INDEX_NAME` varchar(64) DEFAULT NULL,
  `COUNT_STAR` bigint(20) unsigned NOT NULL,
  `SUM_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `MIN_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `AVG_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `MAX_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `COUNT_READ` bigint(20) unsigned NOT NULL,
  `SUM_TIMER_READ` bigint(20) unsigned NOT NULL,
  `MIN_TIMER_READ` bigint(20) unsigned NOT NULL,
  `AVG_TIMER_READ` bigint(20) unsigned NOT NULL,
  `MAX_TIMER_READ` bigint(20) unsigned NOT NULL,
  `COUNT_WRITE` bigint(20) unsigned NOT NULL,
  `SUM_TIMER_WRITE` bigint(20) unsigned NOT NULL,
  `MIN_TIMER_WRITE` bigint(20) unsigned NOT NULL,
  `AVG_TIMER_WRITE` bigint(20) unsigned NOT NULL,
  `MAX_TIMER_WRITE` bigint(20) unsigned NOT NULL,
  `COUNT_FETCH` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_FETCH` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_FETCH` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_FETCH` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_FETCH` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_INSERT` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_UPDATE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_UPDATE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_UPDATE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_UPDATE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_UPDATE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `COUNT_DELETE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `SUM_TIMER_DELETE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MIN_TIMER_DELETE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `AVG_TIMER_DELETE` bigint(20) unsigned NOT NULL  DEFAULT '0',
  `MAX_TIMER_DELETE` bigint(20) unsigned NOT NULL  DEFAULT '0'
);
CREATE index idx_tiwsbiu_app_snap on ps_table_io_waits_summary_by_index_usage(appname,snap_id);
CREATE index idx_tiwsbiu_log_time on ps_table_io_waits_summary_by_index_usage(log_time);

CREATE table ps_events_statements_summary_by_digest 
(
    hostname varchar(64),appname varchar(64),log_time datetime,snap_id int,
  `SCHEMA_NAME` varchar(64) DEFAULT NULL,
  `DIGEST` varchar(32) DEFAULT NULL,
  `DIGEST_TEXT` longtext,
  `COUNT_STAR` bigint(20) unsigned NOT NULL,
  `SUM_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `MIN_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `AVG_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `MAX_TIMER_WAIT` bigint(20) unsigned NOT NULL,
  `SUM_LOCK_TIME` bigint(20) unsigned NOT NULL,
  `SUM_ERRORS` bigint(20) unsigned NOT NULL,
  `SUM_WARNINGS` bigint(20) unsigned NOT NULL,
  `SUM_ROWS_AFFECTED` bigint(20) unsigned NOT NULL,
  `SUM_ROWS_SENT` bigint(20) unsigned NOT NULL,
  `SUM_ROWS_EXAMINED` bigint(20) unsigned NOT NULL,
  `SUM_CREATED_TMP_DISK_TABLES` bigint(20) unsigned NOT NULL,
  `SUM_CREATED_TMP_TABLES` bigint(20) unsigned NOT NULL,
  `SUM_SELECT_FULL_JOIN` bigint(20) unsigned NOT NULL,
  `SUM_SELECT_FULL_RANGE_JOIN` bigint(20) unsigned NOT NULL,
  `SUM_SELECT_RANGE` bigint(20) unsigned NOT NULL,
  `SUM_SELECT_RANGE_CHECK` bigint(20) unsigned NOT NULL,
  `SUM_SELECT_SCAN` bigint(20) unsigned NOT NULL,
  `SUM_SORT_MERGE_PASSES` bigint(20) unsigned NOT NULL,
  `SUM_SORT_RANGE` bigint(20) unsigned NOT NULL,
  `SUM_SORT_ROWS` bigint(20) unsigned NOT NULL,
  `SUM_SORT_SCAN` bigint(20) unsigned NOT NULL,
  `SUM_NO_INDEX_USED` bigint(20) unsigned NOT NULL,
  `SUM_NO_GOOD_INDEX_USED` bigint(20) unsigned NOT NULL,
  `FIRST_SEEN` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_SEEN` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);
CREATE index idx_digest_app_snap on ps_events_statements_summary_by_digest(appname,snap_id);
create index idx_ps_essbd_log_time on ps_events_statements_summary_by_digest(log_time);

CREATE table is_index_statistics
(
   hostname varchar(64),appname varchar(64),log_time datetime,snap_id int,
  `TABLE_SCHEMA` varchar(192) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(192) NOT NULL DEFAULT '',
  `INDEX_NAME` varchar(192) NOT NULL DEFAULT '',
  `ROWS_READ` bigint(21) unsigned NOT NULL DEFAULT '0'
);
CREATE index idx_index_statistics_app_snap on is_index_statistics(appname,snap_id);
CREATE index idx_index_statistics_log_time on is_index_statistics(log_time);
 
CREATE table is_table_statistics 
(
    hostname varchar(64),appname varchar(64),log_time datetime,snap_id int,
  `TABLE_SCHEMA` varchar(192) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(192) NOT NULL DEFAULT '',
  `ROWS_READ` bigint(21) unsigned NOT NULL DEFAULT '0',
  `ROWS_CHANGED` bigint(21) unsigned NOT NULL DEFAULT '0',
  `ROWS_CHANGED_X_INDEXES` bigint(21) unsigned NOT NULL DEFAULT '0'
);
CREATE index idx_table_statistics_app_snap on is_table_statistics(appname,snap_id);
CREATE index idx_table_statistics_log_time on is_table_statistics(log_time);
 
CREATE table is_innodb_buffer_pool_stats(hostname varchar(64),appname varchar(64),log_time datetime,snap_id int,
free_buffers bigint unsigned NOT NULL DEFAULT '0',
database_pages bigint unsigned NOT NULL DEFAULT '0',
old_database_pages bigint unsigned NOT NULL DEFAULT '0');
CREATE index idx_buffer_pool_stats_app_snap on is_innodb_buffer_pool_stats(appname,snap_id);

DROP TABLE IF EXISTS is_slave_status;
CREATE TABLE is_slave_status
(hostname VARCHAR(64),appname VARCHAR(64),log_time DATETIME,snap_id INT,
 slave_io_state VARCHAR(128) NOT NULL DEFAULT ' ',
 _master_host VARCHAR(128) NOT NULL DEFAULT ' ',
 _master_user VARCHAR(128) NOT NULL DEFAULT ' ',
 _master_port VARCHAR(128) NOT NULL DEFAULT ' ',
 connect_retry VARCHAR(128) NOT NULL DEFAULT ' ',
 _master_log_file VARCHAR(128) NOT NULL DEFAULT ' ',
 read_master_log_pos VARCHAR(128) NOT NULL DEFAULT ' ',
 _relay_log_file VARCHAR(128) NOT NULL DEFAULT ' ',
 _relay_log_pos VARCHAR(128) NOT NULL DEFAULT ' ',
 relay_master_log_file VARCHAR(128) NOT NULL DEFAULT ' ',
 slave_io_running VARCHAR(128) NOT NULL DEFAULT ' ',
 slave_sql_running VARCHAR(128) NOT NULL DEFAULT ' ',
 replicate_do_db VARCHAR(128) NOT NULL DEFAULT ' ',
 replicate_ignore_db VARCHAR(128) NOT NULL DEFAULT ' ',
 replicate_do_table VARCHAR(128) NOT NULL DEFAULT ' ',
 replicate_ignore_table VARCHAR(128) NOT NULL DEFAULT ' ',
 replicate_wild_do_table VARCHAR(128) NOT NULL DEFAULT ' ',
 replicate_wild_ignore_table VARCHAR(128) NOT NULL DEFAULT ' ',
 last_errno VARCHAR(128) NOT NULL DEFAULT ' ',
 last_error VARCHAR(128) NOT NULL DEFAULT ' ',
 skip_counter VARCHAR(128) NOT NULL DEFAULT ' ',
 exec_master_log_pos VARCHAR(128) NOT NULL DEFAULT ' ',
 relay_log_space VARCHAR(128) NOT NULL DEFAULT ' ',
 until_condition VARCHAR(128) NOT NULL DEFAULT ' ',
 until_log_file VARCHAR(128) NOT NULL DEFAULT ' ',
 until_log_pos VARCHAR(128) NOT NULL DEFAULT ' ',
 seconds_behind_master VARCHAR(128) NOT NULL DEFAULT ' ',
 last_io_errno VARCHAR(128) NOT NULL DEFAULT ' ',
 last_io_error VARCHAR(128) NOT NULL DEFAULT ' ',
 last_sql_errno VARCHAR(128) NOT NULL DEFAULT ' ',
 last_sql_error VARCHAR(128) NOT NULL DEFAULT ' ',
 replicate_ignore_server_ids VARCHAR(128) NOT NULL DEFAULT ' ',
 _master_server_id VARCHAR(128) NOT NULL DEFAULT ' ',
 master_uuid VARCHAR(128) NOT NULL DEFAULT ' ',
 master_info_file VARCHAR(128) NOT NULL DEFAULT ' ',
 sql_delay VARCHAR(128) NOT NULL DEFAULT ' ',
 sql_remaining_delay VARCHAR(128) NOT NULL DEFAULT ' ',
 slave_sql_running_state VARCHAR(128) NOT NULL DEFAULT ' ',
 master_retry_count VARCHAR(128) NOT NULL DEFAULT ' ',
 _master_bind VARCHAR(128) NOT NULL DEFAULT ' ',
 last_io_error_timestamp VARCHAR(128) NOT NULL DEFAULT ' ',
 last_sql_error_timestamp VARCHAR(128) NOT NULL DEFAULT ' ',
 retrieved_gtid_set VARCHAR(128) NOT NULL DEFAULT ' ',
 executed_gtid_set VARCHAR(128) NOT NULL DEFAULT ' ',
 auto_position VARCHAR(128) NOT NULL DEFAULT ' ',
 replicate_rewrite_db VARCHAR(128) NOT NULL DEFAULT ' ',
 channel_name VARCHAR(128) NOT NULL DEFAULT ' ');
CREATE INDEX idx_slave_status_app_snap ON is_slave_status(appname,snap_id);
CREATE INDEX idx_slave_status_log_time ON is_slave_status(log_time);

CREATE table os_info (
	hostname VARCHAR(64),appname VARCHAR(64),log_time DATETIME,snap_id INT,
	cores int not null default 0,
	system_time bigint not null default 0,
	user_time bigint not null default 0,
	io_time bigint not null default 0,
	idle_time bigint not null default 0,
	up_time bigint not null default 0,
	mem_total bigint not null default 0,
	mem_used bigint not null default 0,
	swap_total bigint not null default 0,
	swap_used bigint not null default 0);
CREATE INDEX idx_os_info_app_snap ON os_info(appname,snap_id);
CREATE INDEX idx_os_info_log_time ON os_info(log_time);

CREATE table slow_query_log_digest (
	hostname VARCHAR(64),appname VARCHAR(64),log_time DATETIME,snap_id INT,
	query_id VARCHAR(32) not null default ' ',
	total_response_time decimal not null default 0.0,
	calls bigint not null default 0,
	abbr_sql VARCHAR(256) not null default ' ',
	full_sql VARCHAR(1024) not null default ' ',
	total_rows_examined bigint not null default 0,
	total_rows_effected bigint not null default 0);
CREATE INDEX idx_slow_query_log_digest_app_snap ON slow_query_log_digest(appname,snap_id);
CREATE INDEX idx_slow_query_log_digest_log_time ON slow_query_log_digest(log_time);

CREATE USER perf_stat@'%' IDENTIFIED BY 'hundsun';
GRANT ALL ON *.* TO perf_stat@'%' ;
grant super on *.* to perf_stat@'%';
